package Utilities;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import jdk.nashorn.internal.parser.JSONParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class JSONservice {

    /*
    * JSON Format :
    *
    * [
    *    {
    *       id : "CA83935398092r2"
    *
    *    } ,
    *    {
    *       id : "CAjlkj98nkj89"
    *    }
    *    ...
    * ]
    *
    * */

    public static HashMap JSONarrayParser(JsonArray arr)
    {
        HashMap<String,HashMap<String,String>> finalMap = new HashMap<>();

        for(JsonElement ele : arr.getAsJsonArray()) {

            HashMap<String,String> map = new HashMap<>();
            JsonObject obj = ele.getAsJsonObject();

            for (String key : obj.keySet()) {
                map.put(key,obj.get(key).getAsString());
            }

            finalMap.put(obj.get("id").getAsString(),map);
        }

        return finalMap;
    }


    /*
  * JSON Format :
  *
  *    {
  *       id : "CA83935398092r2",
  *       ...
  *    }
  *
  * */

    public static HashMap JSONobjectParser (JsonObject obj){

        HashMap<String,String> finalMap = new HashMap<>();

        for (String key : obj.keySet()) {
                finalMap.put(key,obj.get(key).getAsString());
        }

        return finalMap;
    }

    public static JsonArray mergeJsonArrays(JsonArray arr1 , JsonArray arr2){
        JsonArray finalArr = new JsonArray();
        for (JsonElement elem : arr1) {
            finalArr.add(elem);
        }

        for (JsonElement elem : arr2) {
            finalArr.add(elem);
        }

        return finalArr;
    }

    public static HashMap<String, ArrayList<String>> JSONinnerArrayParser(JsonObject obj) {
        HashMap<String, ArrayList<String>> finalMap = new HashMap<>();
        ArrayList<String> list = new ArrayList<>();
        for (String n : obj.keySet()) {
            if(obj.get(n).isJsonArray()){
                JsonArray arr = obj.get(n).getAsJsonArray();
                int i = 0;
                for (JsonElement elem : arr)
                {
                    list.add(i,elem.getAsString());
                    i++;
                }
                finalMap.put(n,list);
            }
            else {
                list.add(0,obj.get(n).getAsString());
                finalMap.put(n,list);
            }
            list = new ArrayList<>();
        }
        return finalMap;
    }

    public static String getString(Object str){
        if(str != null)
        {
            return str.toString().
                    substring(1, str.toString().length()-1);
        }
        return null;
    }
}