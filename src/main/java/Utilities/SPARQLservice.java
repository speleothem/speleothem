package Utilities;

import com.google.gson.*;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.query.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static SpeleothemStore.TripleStoreConfig.conn;

public class SPARQLservice {

    public static final String prefixes=
            "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
                    "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n" +
                    "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n" +
                    "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>\n" +
                    "PREFIX : <http://www.speleothem.org/vocabulary/alpha/speleothem#>\n";

    public static void insertData(String query)
    {
        String update = prefixes + query;
        Update operation = conn.prepareUpdate(QueryLanguage.SPARQL, update);
        operation.execute();
    }

    public static JsonObject getResults(String queryString){
        JsonObject obj = new JsonObject();
        TupleQuery query = conn.prepareTupleQuery(queryString);
        try (TupleQueryResult result = query.evaluate()) {
            while (result.hasNext()) {
                BindingSet solution = result.next();
                Set name = solution.getBindingNames();
                for(Object n : name)
                {
                    if(obj.has((String) n)) {
                        if(obj.get((String) n).isJsonArray()){
                            boolean found = false;
                            JsonArray arr2 = obj.get((String) n).getAsJsonArray();
                            for (JsonElement it : arr2) {
                                if(it.getAsString().equals(solution.getValue((String) n).stringValue()))
                                {
                                    found = true;
                                }
                            }
                            if(!found){
                                arr2.add(solution.getValue((String) n).stringValue());
                                obj.add((String) n, arr2);
                            }
                        } else {
                            if (!obj.get((String) n).getAsString().equals(solution.getValue((String) n).stringValue())) {
                                String temp = obj.get((String) n).getAsString();
                                obj.remove((String) n);
                                JsonArray arr = new JsonArray();
                                arr.add(temp);
                                arr.add(solution.getValue((String) n).stringValue());
                                obj.add((String) n, arr);
                            }
                        }
                    }
                    else
                    {
                        if(solution.getValue((String) n) != null)
                            obj.addProperty((String)n,solution.getValue((String) n).stringValue());
                    }
                }
            }
        }
        return obj;
    }

    public static boolean getAskResults(String queryString){

        BooleanQuery query = conn.prepareBooleanQuery(queryString);

        return  query.evaluate();

    }

    public static JsonArray getResultsAsArray(String queryString){

        JsonArray arr = new JsonArray();
        JsonObject obj;
        TupleQuery query = conn.prepareTupleQuery(queryString);

        try (TupleQueryResult result = query.evaluate()) {
            obj = new JsonObject();
            while (result.hasNext()) {

                BindingSet solution = result.next();
                Set name = solution.getBindingNames();

                for(Object n : name)
                {
                    obj.addProperty((String)n,solution.getValue((String) n).stringValue());
                }
                arr.add(obj);
                obj = new JsonObject();
            }
        }
        return arr;
    }

}