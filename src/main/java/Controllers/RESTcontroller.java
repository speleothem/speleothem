package Controllers;

import SpeleothemStore.TripleStoreConfig;
import Users.*;
import Users.UserService;
import Utilities.JSONservice;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import spark.Request;
import spark.Response;

import javax.servlet.MultipartConfigElement;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;

import static spark.Spark.*;

public class RESTcontroller {
    public static void main(String [] args) throws IOException {

        staticFiles.location("/public");
        TripleStoreConfig.loadModel();

//        post(":id/:fileName/upload", (request, response) -> {
//            JsonElement jelement = new JsonParser().parse(request.body());
//            HashMap map = JSONservice.JSONobjectParser(jelement.getAsJsonObject());
//            System.out.println(map);
//            Cave.addImage(map,request.params("id"));
//            return 1;
//        });

//        post(":id/:fileName/upload", (request, response) -> {
//
//            request.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement(""));
//
//            String userDirectoryName = request.params("id");
//            String fileName = request.params("fileName");
//            String storagePath = "./public/storage";
//
//            try (InputStream is = request.raw().getPart("photo").getInputStream()) {
//                File dir = new File(storagePath+"/"+userDirectoryName);
//                if(!dir.exists()){
//                    boolean create = dir.mkdir();
//                }
//
//                File targetFile = new File(storagePath +
//                        "/"+userDirectoryName+"/"+fileName);
//
//                boolean bool = targetFile.createNewFile();
//                Files.copy(is,targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
//            }
//            JsonObject obj = new JsonObject();
//            obj.addProperty("src","public/storage" +
//                    "/"+userDirectoryName+"/"+fileName);
//            return obj;
//        });

        path("/api", () -> {
            get("/UserProfiles", (Request req, Response res) -> {
                return JSONservice.mergeJsonArrays(Caver.getCavers(),CavingClub.getCavingClubs());
            });
            path("/UserProfiles", () -> {
                post("/Login", (Request req, Response res) -> {

                    JsonElement jelement = new JsonParser().parse(req.body());
                    HashMap map = JSONservice.JSONobjectParser(jelement.getAsJsonObject());
                    if( UserService.loginValidation(map.get("username").toString(),
                            map.get("password").toString())){
                        return UserService.getUserByUsername(map.get("username").toString());
                    }
                    JsonObject obj = new JsonObject();
                    obj.addProperty("type","false");
                    return obj;
                });

                get("/Cavers", (Request req, Response res) -> {
                    System.out.println(Caver.getCavers());
                    return Caver.getCavers();
                });
                get("/CavingClubs", (Request req, Response res) -> {
                    return CavingClub.getCavingClubs();
                });
                path("/CavingClubs", () -> {

                    post("/SignUp", (Request req, Response res) -> {

                        JsonElement jelement = new JsonParser().parse(req.body());
                        HashMap<String, ArrayList<String>> map = JSONservice.JSONinnerArrayParser(jelement.getAsJsonObject());
//                        boolean flag;
//                        flag = map.get("admin").toString().equals("") ||
//                                UserService.usernameExist(map.get("admin").toString());

                        if (!UserService.signUpValidation(map)) {
                            return false;
                        }

                        CavingClub.addCavingClub(map);
                        if(map.containsKey("admin")){
                            for (Object admin : map.get("admin").toArray()) {
                                CavingClub.setAdmin(map,(String)admin);
                            }
                        }

                        return true;

                    });

                    path("/:id", () -> {

                        get("/members", (Request req, Response res) -> {
                            System.out.println(CavingClub.getMembers(req.params("id")));
                            return CavingClub.getMembers(req.params("id"));
                        });

                        get("/general", (Request req, Response res) -> {
                            return CavingClub.getCavingClubGeneral(req.params("id"));
                        });

                        get("/expeditions", (Request req, Response res) -> {
                            System.out.println("edo");
                            System.out.println(Cave.getExpeditions(req.params("id")));
                            return Cave.getExpeditions(req.params("id"));
                        });

                        post("/addEquipment",(Request req,Response res) ->{
                            JsonElement jelement = new JsonParser().parse(req.body());
                            HashMap map = JSONservice.JSONobjectParser(jelement.getAsJsonObject());
                            System.out.println(map);
                            UserService.addEquipmentItem(map,req.params("id"));
                            return 1;
                        });

                        get("/Equipment", (Request req, Response res) -> {
                            return UserService.getEquipment(req.params("id"));
                        });


                        post("/addRiggingEquipment",(Request req,Response res) ->{
                            JsonElement jelement = new JsonParser().parse(req.body());
                            HashMap map = JSONservice.JSONobjectParser(jelement.getAsJsonObject());
                            System.out.println(map);;
                            UserService.addRiggingEquipmentItem(map,req.params("id"));
                            return 1;
                        });

                        get("/RiggingEquipment", (Request req, Response res) -> {
                            System.out.println(UserService.getRiggingEquipment(req.params("id")));
                            return UserService.getRiggingEquipment(req.params("id"));
                        });

                    });

                });

                path("/Cavers", () -> {

                    post("/SignUp", (Request req, Response res) -> {
                        JsonElement jelement = new JsonParser().parse(req.body());
                        System.out.println(req.body());
                        HashMap map = JSONservice.JSONobjectParser(jelement.getAsJsonObject());
//                        boolean flag;
//                        flag = map.get("participatingClub").toString().equals("") ||
//                               UserService.usernameExist(map.get("participating club").toString()) ;

                        if (!UserService.signUpValidation(map)) {
                            return false;
                        }

                        Caver.addCaver(map);
                        return true;

                    });

                    get("/:id", (Request req, Response res) -> {
                        return UserService.getUserByID(req.params("id"));
                    });

                    delete("/:id", (Request req, Response res) -> {
                        return "delete";
                    });

                    path("/:id", () -> {

                        get("/general", (Request req, Response res) -> {
//                            System.out.println(Caver.getCaverGeneral(req.params("id")));
                            return Caver.getCaverGeneral(req.params("id"));
                        });

                        post("/addLogbook",(Request req,Response res) ->{
                            JsonElement jelement = new JsonParser().parse(req.body());
                            HashMap<String, ArrayList<String>> map = JSONservice.JSONinnerArrayParser(jelement.getAsJsonObject());
                            Caver.addLogbook(map,req.params("id"));
                            return 1;
                        });

                        get("/logbook", (Request req, Response res) -> {
                            return Caver.getLogbook(req.params("id"));
                        });

                        post("/addEquipment",(Request req,Response res) ->{
                            JsonElement jelement = new JsonParser().parse(req.body());
                            HashMap map = JSONservice.JSONobjectParser(jelement.getAsJsonObject());
                            UserService.addEquipmentItem(map,req.params("id"));
                            return 1;
                        });

                        get("/Equipment", (Request req, Response res) -> {
                            return UserService.getEquipment(req.params("id"));
                        });


                        post("/addRiggingEquipment",(Request req,Response res) ->{
                            JsonElement jelement = new JsonParser().parse(req.body());
                            HashMap map = JSONservice.JSONobjectParser(jelement.getAsJsonObject());
                            System.out.println(map);
//                            UserService.createRiggingEquipment(req.params("id"));
                            UserService.addRiggingEquipmentItem(map,req.params("id"));
                            return 1;
                        });

                        get("/RiggingEquipment", (Request req, Response res) -> {
                            System.out.println(UserService.getRiggingEquipment(req.params("id")));
                            return UserService.getRiggingEquipment(req.params("id"));
                        });

                    });

                });
            });
            get("/Caves", (Request req, Response res) -> {
                return Cave.getCaves();
            });
            path("/Caves", () -> {

                post("/SignUp", (Request req, Response res) -> {
                    JsonElement jelement = new JsonParser().parse(req.body());
                    HashMap map = JSONservice.JSONobjectParser(jelement.getAsJsonObject());
                    Cave.addCave(map);
                    return true;
                });

                get("/:id", (Request req, Response res) -> {
                    return 1;
                });
                path("/:id", () -> {

                    get("/photos", (Request req, Response res) -> {
                        System.out.println(Cave.getImage(req.params("id")));
                        return Cave.getImage(req.params("id"));
                    });

                    get("/sketches", (Request req, Response res) -> {
                        System.out.println(Cave.getSketch(req.params("id")));
                        return Cave.getSketch(req.params("id"));
                    });

                    get("/general", (Request req, Response res) -> {
                        System.out.println(Cave.getCaveGeneral(req.params("id")));
                        return Cave.getCaveGeneral(req.params("id"));
                    });

                    post("/entrances", (Request req, Response res) -> {
                        JsonElement jelement = new JsonParser().parse(req.body());
                        HashMap<String, ArrayList<String>> map = JSONservice.JSONinnerArrayParser(jelement.getAsJsonObject());
                        Cave.createEntrance(map);
                        Cave.addEntrance(UserService.getArrayList(map,"id"),req.params("id"));
                        if(map.containsKey("photos")){
                            HashMap<String,String> map2 = new HashMap<>();
                            String[] keys = new String[]{"link","imageType","id","ownerId","entityId","caveId"};
                            int i = 0;

                            for (Object photo : map.get("photos").toArray()) {
                                map2.put(keys[i],(String)photo);
                                i++;
                            }
                            Cave.createImage(map2);
                            Cave.addImage(map2,map2.get("entityId"));
                            Cave.addImage(map2,map2.get("ownerId"));
                            Cave.addImage(map2,map2.get("caveId"));;
                        }
                        return true;
                    });

                    get("/entrances", (Request req, Response res) -> {
                        System.out.println(Cave.getEntrances(req.params("id")));
                        return Cave.getEntrances(req.params("id"));
                    });

                    post("/climate", (Request req, Response res) -> {
                        JsonElement jelement = new JsonParser().parse(req.body());
                        HashMap map = JSONservice.JSONobjectParser(jelement.getAsJsonObject());
                        System.out.println(map);
                        Cave.createClimate(map);
                        Cave.addClimate(map.get("id").toString(),req.params("id"));
                        return true;
                    });

                    get("/climate", (Request req, Response res) -> {
                        return Cave.getClimate(req.params("id"));
                    });

                    post("/rooms", (Request req, Response res) -> {
                        JsonElement jelement = new JsonParser().parse(req.body());
                        System.out.println(jelement);
                        HashMap<String, ArrayList<String>> map = JSONservice.JSONinnerArrayParser(jelement.getAsJsonObject());
                        Cave.createRoom(map);
                        Cave.addRoom(UserService.getArrayList(map,"id"),req.params("id"));
                        if(map.containsKey("speleothems")){
                            for (Object speleothem : map.get("speleothems").toArray()) {
                                System.out.println(speleothem);
                                Cave.addSpeleothem((String)speleothem,UserService.getArrayList(map,"id"));
                                Cave.addRoom(UserService.getArrayList(map,"id"),(String)speleothem);
                            }
                        }
                        if(map.containsKey("organisms")){
                            for (Object organism : map.get("organisms").toArray()) {
                                System.out.println(organism);
                                Cave.addOrganism((String)organism,UserService.getArrayList(map,"id"));
                                Cave.addRoom(UserService.getArrayList(map,"id"),(String)organism);
                            }
                        }
                        if(map.containsKey("photos")){
                            HashMap<String,String> map2 = new HashMap<>();
                            String[] keys = new String[]{"link","imageType","id","ownerId","entityId","caveId"};
                            int i = 0;

                            for (Object photo : map.get("photos").toArray()) {
                                map2.put(keys[i],(String)photo);
                                i++;
                            }
                            Cave.createImage(map2);
                            Cave.addImage(map2,map2.get("entityId"));
                            Cave.addImage(map2,map2.get("ownerId"));
                            Cave.addImage(map2,map2.get("caveId"));
                        }

                        if(map.containsKey("sketches")){
                            HashMap<String,String> map2 = new HashMap<>();
                            String[] keys = new String[]{"link","sketchType","id","ownerId","entityId","caveId"};
                            int i = 0;

                            for (Object photo : map.get("sketches").toArray()) {
                                map2.put(keys[i],(String)photo);
                                i++;
                            }
                            Cave.createSketch(map2);
                            Cave.addSketch(map2,map2.get("entityId"));
                            Cave.addSketch(map2,map2.get("ownerId"));
                            Cave.addSketch(map2,map2.get("caveId"));;
                        }
                        System.out.println(map);
                        return true;
                    });

                    post("/expeditions", (Request req, Response res) -> {
                        JsonElement jelement = new JsonParser().parse(req.body());
                        HashMap<String, ArrayList<String>> map = JSONservice.JSONinnerArrayParser(jelement.getAsJsonObject());
                        Cave.createExpedition(map);
                        Cave.addExpedition(UserService.getArrayList(map,"id"),req.params("id"));
                        if(map.containsKey("cavers")){
                            for (Object cavers : map.get("cavers").toArray()) {
                                System.out.println(cavers);
                                Cave.addCaver((String)cavers,UserService.getArrayList(map,"id"));
                                Cave.addExpedition(UserService.getArrayList(map,"id"),(String)cavers);
                            }
                        }
                        if(map.containsKey("Club")){
                            String str;
                            for (Object club : map.get("Club").toArray()) {
                                System.out.println(club);
                                str = UserService.getUserByUsername(club.toString()).get("id").toString();
                                str = str.substring(1, str.length()-1);
                                Cave.addCavingClub(str,UserService.getArrayList(map,"id"));
                                Cave.addExpedition(UserService.getArrayList(map,"id"),str);
                            }
                        }
                        if(map.containsKey("riggings")){
                            for (Object riggings : map.get("riggings").toArray()) {
                                System.out.println(riggings);
                                Cave.addRigging((String)riggings,UserService.getArrayList(map,"id"));
                                Cave.addExpedition(UserService.getArrayList(map,"id"),(String)riggings);
                            }
                        }
                        if(map.containsKey("Equipment")){
                            System.out.println(map.get("Equipment"));
                            HashMap<String,String> map2 = new HashMap<>();
                            String[] keys = new String[]{ "description","quantity","type","id" };
                            int i = 0;
                            for (Object r : map.get("Equipment").toArray()) {
                                map2.put(keys[i],(String)r);;
                                i++;
                            }
                            UserService.createEquipment(UserService.getArrayList(map,"id"));
                            UserService.addEquipmentItem(map2,UserService.getArrayList(map,"id"));
                        }
                        System.out.println(map);
                        return true;
                    });

                    post("/cavers", (Request req, Response res) -> {
                        JsonElement jelement = new JsonParser().parse(req.body());
                        HashMap<String, ArrayList<String>> map = JSONservice.JSONinnerArrayParser(jelement.getAsJsonObject());
                        System.out.println(map);
                        if(map.get("Username").toString().equals("[]")){

                        }
                        else{
                            JsonElement caver = UserService.getUserByUsername(UserService.getArrayList(map,"Username")).get("id");
                            if(caver != null){
                                Cave.addCaver(caver.getAsString(),req.params("id"));
                                if(map.containsKey("expeditions")){
                                    for (Object expeditions : map.get("expeditions").toArray()) {
                                        System.out.println(expeditions);
                                        Cave.addExpedition((String)expeditions,caver.getAsString());
                                        Cave.addCaver(caver.getAsString(),(String)expeditions);
                                    }
                                }
                            }
                        }
                        return true;
                    });

                    post("/speleothems", (Request req, Response res) -> {
                        JsonElement jelement = new JsonParser().parse(req.body());
                        HashMap<String, ArrayList<String>> map = JSONservice.JSONinnerArrayParser(jelement.getAsJsonObject());
                        System.out.println(map);
                        Cave.createSpeleothem(map);
                        Cave.addSpeleothem(UserService.getArrayList(map,"id"),req.params("id"));
                        if(map.containsKey("expeditions")){
                            for (Object expeditions : map.get("expeditions").toArray()) {
                                System.out.println(expeditions);
                                Cave.addExpedition((String)expeditions,UserService.getArrayList(map,"id"));
                                Cave.addSpeleothem(UserService.getArrayList(map,"id"),(String)expeditions);
                            }
                        }
                        if(map.containsKey("rooms")){
                            for (Object rooms : map.get("rooms").toArray()) {
                                System.out.println(rooms);
                                Cave.addRoom((String)rooms,UserService.getArrayList(map,"id"));
                                Cave.addSpeleothem(UserService.getArrayList(map,"id"),(String)rooms);
                            }
                        }
                        if(map.containsKey("photos")){
                            HashMap<String,String> map2 = new HashMap<>();
                            String[] keys = new String[]{"link","imageType","id","ownerId","entityId","caveId"};
                            int i = 0;

                            for (Object photo : map.get("photos").toArray()) {
                                map2.put(keys[i],(String)photo);
                                i++;
                            }
                            Cave.createImage(map2);
                            Cave.addImage(map2,map2.get("entityId"));
                            Cave.addImage(map2,map2.get("ownerId"));;
                            Cave.addImage(map2,map2.get("caveId"));
                        }
                        System.out.println(map);
                        return true;
                    });

                    post("/organisms", (Request req, Response res) -> {
                        JsonElement jelement = new JsonParser().parse(req.body());
                        HashMap<String, ArrayList<String>> map = JSONservice.JSONinnerArrayParser(jelement.getAsJsonObject());
                        Cave.createOrganism(map);
                        Cave.addOrganism(UserService.getArrayList(map,"id"),req.params("id"));
                        if(map.containsKey("expeditions")){
                            for (Object expeditions : map.get("expeditions").toArray()) {
                                System.out.println(expeditions);
                                Cave.addExpedition((String)expeditions,UserService.getArrayList(map,"id"));
                                Cave.addOrganism(UserService.getArrayList(map,"id"),(String)expeditions);
                            }
                        }
                        if(map.containsKey("rooms")){
                            for (Object rooms : map.get("rooms").toArray()) {
                                System.out.println(rooms);
                                Cave.addRoom((String)rooms,UserService.getArrayList(map,"id"));
                                Cave.addOrganism(UserService.getArrayList(map,"id"),(String)rooms);
                            }
                        }
                        if(map.containsKey("photos")){
                            HashMap<String,String> map2 = new HashMap<>();
                            String[] keys = new String[]{"link","imageType","id","ownerId","entityId","caveId"};
                            int i = 0;

                            for (Object photo : map.get("photos").toArray()) {
                                map2.put(keys[i],(String)photo);
                                i++;
                            }
                            Cave.createImage(map2);
                            Cave.addImage(map2,map2.get("entityId"));;
                            Cave.addImage(map2,map2.get("ownerId"));
                            Cave.addImage(map2,map2.get("caveId"));
                        }
                        System.out.println(map);
                        return true;
                    });

                    post("/riggings", (Request req, Response res) -> {
                        JsonElement jelement = new JsonParser().parse(req.body());
                        HashMap<String, ArrayList<String>> map = JSONservice.JSONinnerArrayParser(jelement.getAsJsonObject());
                        Cave.createRigging(map);
                        Cave.addRigging(UserService.getArrayList(map,"id"),req.params("id"));
                        if(map.containsKey("expeditions")){
                            for (Object expeditions : map.get("expeditions").toArray()) {
                                System.out.println(expeditions);
                                Cave.addExpedition((String)expeditions,UserService.getArrayList(map,"id"));
                                Cave.addRigging(UserService.getArrayList(map,"id"),(String)expeditions);
                            }
                        }
                        if(map.containsKey("RiggingEquipment")){
                                System.out.println(map.get("RiggingEquipment"));
                            HashMap<String,String> map2 = new HashMap<>();
                            String[] keys = new String[]{ "description","quantity","type","id" };
                            int i = 0;
                            for (Object r : map.get("RiggingEquipment").toArray()) {
                                map2.put(keys[i],(String)r);
                                i++;
                            }
                                UserService.createRiggingEquipment(UserService.getArrayList(map,"id"));
                                UserService.addRiggingEquipmentItem(map2,UserService.getArrayList(map,"id"));
                        }
                        if(map.containsKey("Equipment")){
                            System.out.println(map.get("Equipment"));
                            HashMap<String,String> map2 = new HashMap<>();
                            String[] keys = new String[]{ "description","quantity","type","id" };
                            int i = 0;
                            for (Object r : map.get("Equipment").toArray()) {
                                map2.put(keys[i],(String)r);
                                i++;
                            }
                            UserService.createEquipment(UserService.getArrayList(map,"id"));
                            UserService.addEquipmentItem(map2,UserService.getArrayList(map,"id"));
                        }
                        if(map.containsKey("sketches")){
                            HashMap<String,String> map2 = new HashMap<>();
                            String[] keys = new String[]{"link","sketchType","id","ownerId","entityId","caveId"};
                            int i = 0;

                            for (Object photo : map.get("sketches").toArray()) {
                                map2.put(keys[i],(String)photo);
                                i++;
                            }
                            Cave.createSketch(map2);
                            Cave.addSketch(map2,map2.get("entityId"));
                            Cave.addSketch(map2,map2.get("ownerId"));
                            Cave.addSketch(map2,map2.get("caveId"));;
                        }
                        System.out.println(map);
                        return true;
                    });

                    get("/rooms", (Request req, Response res) -> {
                        return Cave.getRooms(req.params("id"));
                    });

                    get("/speleothems", (Request req, Response res) -> {
                        System.out.println(Cave.getSpeleothems(req.params("id")));
                        return Cave.getSpeleothems(req.params("id"));
                    });

                    get("/organisms", (Request req, Response res) -> {
                        System.out.println("geiaaa"+Cave.getOrganisms(req.params("id")));
                        return Cave.getOrganisms(req.params("id"));
                    });

                    get("/expeditions", (Request req, Response res) -> {
                        System.out.println("geia");
                        System.out.println(req.params("id"));
                        System.out.println(Cave.getExpeditions(req.params("id")));
                        return Cave.getExpeditions(req.params("id"));
                    });

                    get("/riggings", (Request req, Response res) -> {
                        System.out.println(Cave.getRiggings(req.params("id")));
                        return Cave.getRiggings(req.params("id"));
                    });

                    get("/cavers", (Request req, Response res) -> {
                        System.out.println(Cave.getCavers(req.params("id")));
                        return Cave.getCavers(req.params("id"));
                    });


                    path("/rooms/:id", () -> {
                        get("/speleothems", (Request req, Response res) -> {
                            System.out.println(req.params("id"));
                            System.out.println(Cave.getSpeleothems(req.params("id")));
                            return Cave.getSpeleothems(req.params("id"));
                        });
                        get("/organisms", (Request req, Response res) -> {
                            return Cave.getOrganisms(req.params("id"));
                        });
                        get("/photos", (Request req, Response res) -> {
                            System.out.println(Cave.getImage(req.params("id")));
                            return Cave.getImage(req.params("id"));
                        });
                        get("/sketches", (Request req, Response res) -> {
                            System.out.println(Cave.getSketch(req.params("id")));
                            return Cave.getSketch(req.params("id"));
                        });
                    });

                    path("/entrances/:id", () -> {
                        get("/photos", (Request req, Response res) -> {
                            System.out.println(Cave.getImage(req.params("id")));
                            return Cave.getImage(req.params("id"));
                        });
                    });

                    path("/speleothems/:id", () -> {
                        get("/rooms", (Request req, Response res) -> {
                            return Cave.getRooms(req.params("id"));
                        });
                        get("/expeditions", (Request req, Response res) -> {
                            return Cave.getExpeditions(req.params("id"));
                        });
                        get("/photos", (Request req, Response res) -> {
                            System.out.println(Cave.getImage(req.params("id")));
                            return Cave.getImage(req.params("id"));
                        });
                    });

                    path("/organisms/:id", () -> {
                        get("/rooms", (Request req, Response res) -> {
                            return Cave.getRooms(req.params("id"));
                        });
                        get("/expeditions", (Request req, Response res) -> {
                            return Cave.getExpeditions(req.params("id"));
                        });
                        get("/photos", (Request req, Response res) -> {
                            System.out.println(Cave.getImage(req.params("id")));
                            return Cave.getImage(req.params("id"));
                        });
                    });

                    path("/expeditions/:id", () -> {
                        get("/riggings", (Request req, Response res) -> {
                            return Cave.getRiggings(req.params("id"));
                        });
                        get("/cavers", (Request req, Response res) -> {
                            return Cave.getCavers(req.params("id"));
                        });
                        get("/Equipment", (Request req, Response res) -> {
                            return UserService.getEquipment(req.params("id"));
                        });
                    });

                    path("/cavers/:id", () -> {
                        get("/expeditions", (Request req, Response res) -> {
                            return Cave.getExpeditions(req.params("id"));
                        });
                    });

                    path("/riggings/:id", () -> {
                        get("/expeditions", (Request req, Response res) -> {
                            return Cave.getExpeditions(req.params("id"));
                        });
                        get("/sketches", (Request req, Response res) -> {
                            System.out.println(Cave.getSketch(req.params("id")));
                            return Cave.getSketch(req.params("id"));
                        });
                        get("/RiggingEquipment", (Request req, Response res) -> {
                            return UserService.getRiggingEquipment(req.params("id"));
                        });
                        get("/Equipment", (Request req, Response res) -> {
                            return UserService.getEquipment(req.params("id"));
                        });
                    });



                });

            });

            path("/Services", () -> {
                get("/Sparql", (Request req, Response res) -> {
                    res.type("application/json");
                    return 1;
                });
            });

        });

    }

}
