package SpeleothemStore;

import com.sun.org.apache.regexp.internal.RE;
import org.eclipse.rdf4j.RDF4JException;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.config.RepositoryConfig;
import org.eclipse.rdf4j.repository.config.RepositoryImplConfig;
import org.eclipse.rdf4j.repository.manager.RemoteRepositoryManager;
import org.eclipse.rdf4j.repository.manager.RepositoryManager;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.sail.config.SailRepositoryConfig;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFHandler;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.config.SailImplConfig;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.eclipse.rdf4j.sail.memory.config.MemoryStoreConfig;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

public class TripleStoreConfig {

    public static RepositoryConnection conn;

    public static void loadModel() throws IOException {
        String filename = "public/ontology/";
        filename += getConfig("ontologyFileName");

        InputStream input = TripleStoreConfig.class.getResourceAsStream("/" + filename);
        Model model = Rio.parse(input, "", RDFFormat.TURTLE);

        RepositoryManager manager = new RemoteRepositoryManager(getConfig("serverUrl"));
        manager.initialize();

        Repository db;
        Boolean modelAlreadyExist = true;

        //create new repository with persistance
        if(manager.getRepository(getConfig("repositoryName")) == null){
            RepositoryConfig repConfig = new RepositoryConfig(getConfig("repositoryName"));
            MemoryStoreConfig memConfig = new MemoryStoreConfig();
            memConfig.setPersist(true);
            SailRepositoryConfig config = new SailRepositoryConfig(memConfig);
            repConfig.setRepositoryImplConfig(config);
            manager.addRepositoryConfig(repConfig);
            modelAlreadyExist = false;
        }

        db = manager.getRepository(getConfig("repositoryName"));
        db.initialize();

        try {
            conn = db.getConnection();
            if(!modelAlreadyExist)
             conn.add(model);
        }
        finally {
            conn.close();
        }

    }

    public static String getConfig(String key){
        Properties prop = new Properties();
        InputStream input = null;
        HashMap<String,String> map = new HashMap<>();
        String filename = "public/ontology/config.properties";

        try {

            input = TripleStoreConfig.class.getResourceAsStream("/" + filename);

            // load a properties file
            prop.load(input);

            for (String name : prop.stringPropertyNames()) {
                map.put(name,prop.getProperty(name));
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return map.get(key);
    }

}