package Users;

import Utilities.SPARQLservice;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.HashMap;

public class Caver
{

    public static void addCaver (HashMap map){
        String query =
        "INSERT DATA { " +
                ":" + map.get("id") +
                " rdf:type owl:NamedIndividual , :Caver ; \n" +
                "       :has_Username \"" + map.get("username") + "\"; \n" +
                "       :has_Email \"" + map.get("email") + "\"; \n" +
                "       :has_First_Name \"" + map.get("firstName") + "\"; \n" +
                "       :has_Last_Name \"" + map.get("lastName") + "\"; \n" +
                "       :has_Gender \"" + map.get("gender") + "\"; \n" +
                "       :has_Image \"" + map.get("profilePhoto") + "\"; \n" +
                "       :can_Participate_In_Cave_Rescue \"" + map.get("resquePartitipation") + "\"; \n";

        if(map.containsKey("participatingClub")){
            query += ":has_Participating_Club "+":" + map.get("participatingClub") + " ; \n";
        }

       query += " :has_ID \"" + map.get("id") + "\"; \n" +
                "       :has_Password \"" + map.get("password") + "\" ." +
                "}";
        SPARQLservice.insertData(query);
        UserService.createEquipment(map.get("id").toString());
        UserService.createRiggingEquipment(map.get("id").toString());
    }

    public  static JsonArray getCavers(){
        String query = SPARQLservice.prefixes;
        query +=
                "select ?username ?Id\n" +
                        "where\n" +
                        "{\n" +
                        "   ?s rdf:type owl:NamedIndividual, :Caver ;\n" +
                        "  :has_ID ?Id ; \n" +
                        "  :has_Username ?username.\n" +
                        "}";
        return SPARQLservice.getResultsAsArray(query);
    }

    public  static JsonObject getCaverGeneral(String id){

        String query = SPARQLservice.prefixes;

        query += "SELECT ?username ?email ?firstName ?lastName ?gender ?resque ?partClub ?admin ?profilePhoto \n";
        query += "WHERE { \n";
        query += "?s rdf:type owl:NamedIndividual ;" +
                " :has_ID \""+id+"\" ; \n" +
                " :has_Username ?username ; \n" +
                " :has_Email ?email ; \n" +
                " :has_First_Name ?firstName ; \n" +
                " :has_Last_Name ?lastName ; \n" +
                " :has_Gender ?gender ; \n" +
                " :can_Participate_In_Cave_Rescue ?resque ; \n" +
                " :has_Image ?profilePhoto . \n" +
                " OPTIONAL { ?s :has_Participating_Club ?partClub }  \n" +
                " OPTIONAL { ?s :is_Admin_At ?admin }  \n";
        query += "}";

        JsonObject obj = SPARQLservice.getResults(query);
        if(obj.has("partClub")){
            UserService.URI2Username(obj,"partClub");
        }

        if(obj.has("admin")){
            UserService.URI2Username(obj,"admin");
        }

        return obj;
    }

    public static void addLogbook(HashMap map,String id){
        String logbook = map.get("logbook").toString();
        logbook = logbook.substring(1, logbook.length()-1);
        logbook = logbook.replace("\n", "[newline]");
        String query =
                "INSERT DATA { " +
                        ":" + id +
                        " rdf:type owl:NamedIndividual ;\n" +
                        "       :has_Logbook \"" + logbook + "\" ." +
                        "}";
        SPARQLservice.insertData(query);

    }

    public static JsonArray getLogbook(String id){
        String queryString = SPARQLservice.prefixes;

        queryString += "SELECT ?logbook \n" +
                "WHERE { \n" +
                " ?s :has_ID \""+id+ "\" ;" +
                "    :has_Logbook ?logbook ." +
                "}";

        return  SPARQLservice.getResultsAsArray(queryString);
    }

}
