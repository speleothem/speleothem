package Users;

import Utilities.SPARQLservice;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

public class Cave {

    public static void addCave (HashMap map){

        String query =
                "INSERT DATA { " +
                        ":" + map.get("id") +
                        " rdf:type owl:NamedIndividual ,\n" +
                        "          :"+Cave.getIndividualType(map.get("caveType").toString())+" ;\n" +
                        " rdfs:subClassOf :Cave ; \n" +
                        "       :has_ID \"" + map.get("id") + "\"; \n" +
                        "       :has_Name \"" + map.get("name") + "\"; \n" +
                        "       :has_External_Source \"" + Cave.getExternalSource(map.get("caveType").toString()) + "\"; \n" +
                        "       :has_Description \"" + map.get("description") + "\"; \n" +
                        "       :has_Catalogue_Name \"" + map.get("catalogueName") + "\"; \n" +
                        "       :has_Local_Name \"" + map.get("localName") + "\"; \n" +
                        "       :has_Other_Name \"" + map.get("otherName") + "\"; \n" +
                        "       :has_Location \"" + map.get("location") + "\"; \n" +
                        "       :has_Province \"" + map.get("province") + "\"; \n" +
                        "       :has_Administrative_Region \"" + map.get("administrativeRegion") + "\"; \n" +
                        "       :has_Country \"" + map.get("country") + "\"; \n" +
                        "       :has_Municipality \"" + map.get("municipality") + "\"; \n" +
                        "       :has_Length \"" + map.get("length") + "\"; \n" +
                        "       :has_Depth \"" + map.get("depth") + "\"; \n" +
                        "       :has_Altitude \"" + map.get("altitude") + "\"; \n" +
                        "       :has_Settlement \"" + map.get("settlement") + "\"; \n" +
                        "       :has_Usage \"" + map.get("usage") + "\"; \n" +
                        "       :latitude \"" + map.get("latitude") + "\"; \n" +
                        "       :longitude \"" + map.get("longtitude") + "\"; \n" +
                        "       :has_Cave_Type \"" + map.get("caveType") + "\"; \n" +
                        "       :known_From \"" + map.get("knownFrom") + "\"; \n" +
                        "       :has_City \"" + map.get("city") + "\"; \n" +
                        "       :has_Image \"" + map.get("profilePhoto") + "\"; \n" +
                        "       :is_Visitable \"" + map.get("visitable") + "\". \n" +
                        "}";
        SPARQLservice.insertData(query);
    }

    public static String getIndividualType(String caveType){
        switch (caveType) {
            case "ice cave":
                return "Ice_caves";
            case "karst cave":
                return "Karst_caves";
            case "sea cave":
                return "Sea_caves";
            case "show cave":
                return "Show_caves";
            case "wild cave":
                return "Wild_caves";
            case "Troglofauna":
                return "Troglofauna";
            case "Troglomorphism":
                return "Troglomorphism";
            case "Trogloxene":
                return "Trogloxene";
            case "Stalagmite":
                return "Stalagmite";
            case "Stalactite":
                return "Stalactite";
            case "Curtain":
                return "Curtain";
            default:
                return "Other";
        }
    }

    public static String getExternalSource(String type){
        if(type.equals("ice cave") || type.equals("karst cave") || type.equals("sea cave")
                || type.equals("show cave") || type.equals("wild cave"))
                return "http://live.dbpedia.org/page/Category:"+Cave.getIndividualType(type);
        else if(type.equals("Troglofauna") || type.equals("Troglomorphism") || type.equals("Trogloxene"))
            return "http://dbpedia.org/page/"+Cave.getIndividualType(type);
        else if(type.equals("Stalagmite") || type.equals("Stalactite"))
            return "http://dbpedia.org/page/"+Cave.getIndividualType(type);
        else
            return "";
    }

    public static JsonObject getCaveGeneral(String id){
        String queryString = SPARQLservice.prefixes;

        queryString += "SELECT *  \n" +
               "WHERE { \n" +
                 " ?s rdf:type owl:NamedIndividual ;" +
                 " :has_ID \"" + id + "\" ; \n" +
                 " :has_Name ?name ; \n" +
                 " :has_Description ?description ; \n" +
                 " :has_Catalogue_Name ?catalogueName ; \n" +
                 " :has_Local_Name ?localName ; \n" +
                 " :has_Other_Name ?otherName ; \n" +
                 " :has_Location ?location ; \n" +
                 " :has_Province ?province; \n" +
                 " :has_Length ?length; \n" +
                 " :has_Depth ?depth; \n" +
                 " :has_Altitude ?altitude; \n" +
                 " :has_Administrative_Region ?administrativeRegion; \n" +
                 " :has_City ?city; \n" +
                 " :has_Country ?country; \n" +
                 " :has_Municipality ?municipality; \n" +
                 " :has_Settlement ?settlement; \n" +
                 " :latitude ?latitude; \n" +
                 " :longitude ?longitude; \n" +
                 " :has_Cave_Type ?caveType; \n" +
                 " :has_External_Source ?externalSource; \n" +
                 " :known_From ?knownFrom; \n" +
                "  :has_Image ?profilePhoto ; \n" +
                "  :is_Visitable ?visitable. \n" +
                 "}";

        return  SPARQLservice.getResults(queryString);

    }

    public  static JsonArray getCaves(){
        String query = SPARQLservice.prefixes;
        query +=
                "select *\n" +
                        "where\n" +
                        "{\n" +
                        "   ?s rdfs:subClassOf :Cave ;\n" +
                        "  :has_ID ?Id ; \n" +
                        "  :has_Description ?description ; \n" +
                        "  :has_Length ?length; \n" +
                        "  :has_Depth ?depth; \n" +
                        "  :has_Altitude ?altitude; \n" +
                        "  :latitude ?latitude; \n" +
                        "  :longitude ?longitude; \n" +
                        "  :has_Name ?name.\n" +
                        "}";
        return SPARQLservice.getResultsAsArray(query);
    }

    public static void createImage(HashMap map){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        LocalDate localDate = LocalDate.now();
        String query =
                "INSERT DATA { " +
                        ":" + map.get("id") +
                        " rdf:type owl:NamedIndividual , :ImageObject ;" +
                        " rdfs:subClassOf :Images ; \n" +
                        " :has_Type \"" +map.get("imageType")+"\" ;"+
                        " :has_ID \"" +map.get("id")+"\" ;"+
                        " :has_Owner :" +map.get("ownerId")+" ;"+
                        " :has_Date \"" +dtf.format(localDate)+"\" ;"+
                        " :has_Src \"" +map.get("link")+"\" ."+
                        "}";
        SPARQLservice.insertData(query);
    }

    public static void createSketch(HashMap map){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        LocalDate localDate = LocalDate.now();
        String query =
                "INSERT DATA { " +
                        ":" + map.get("id") +
                        " rdf:type owl:NamedIndividual , :ImageObject ;" +
                        " rdfs:subClassOf :Sketches ; \n" +
                        " :has_Type \"" +map.get("sketchType")+"\" ;"+
                        " :has_ID \"" +map.get("id")+"\" ;"+
                        " :has_Owner :" +map.get("ownerId")+" ;"+
                        " :has_Date \"" +dtf.format(localDate)+"\" ;"+
                        " :has_Src \"" +map.get("link")+"\" ."+
                        "}";
        SPARQLservice.insertData(query);
    }

    public static void addImage(HashMap map,String id){
//        createImage(map);
        String query =
                "INSERT DATA { " +
                        ":" + id +
                        " rdf:type owl:NamedIndividual ;\n" +
                        "       :has_Image :" + map.get("id") + " ." +
                        "}";
        SPARQLservice.insertData(query);

    }

    public static void addSketch(HashMap map,String id){
//        createImage(map);
        String query =
                "INSERT DATA { " +
                        ":" + id +
                        " rdf:type owl:NamedIndividual ;\n" +
                        "       :has_Sketch :" + map.get("id") + " ." +
                        "}";
        SPARQLservice.insertData(query);

    }

    public static JsonArray getImage(String id){
        String queryString = SPARQLservice.prefixes;
        queryString += "SELECT ?link ?imageType ?id ?ownerId ?username ?date \n" +
                "WHERE { \n" +
                " ?s :has_ID \""+id+ "\" ;" +
                "    :has_Image ?profilePhoto ." +
                " ?profilePhoto :has_Src ?link ;" +
                "               :has_ID ?id ;" +
                "               :has_Type ?imageType ;" +
                "               :has_Date ?date ;" +
                "               :has_Owner ?ownerId ." +
                " ?ownerId      :has_Username ?username ." +
                "}";

        return  SPARQLservice.getResultsAsArray(queryString);
    }

    public static JsonArray getSketch(String id){
        String queryString = SPARQLservice.prefixes;
        queryString += "SELECT ?link ?sketchType ?id ?ownerId ?username ?date\n" +
                "WHERE { \n" +
                " ?s :has_ID \""+id+ "\" ;" +
                "    :has_Sketch ?sketch ." +
                " ?sketch :has_Src ?link ;" +
                "               :has_ID ?id ;" +
                "               :has_Type ?sketchType ;" +
                "               :has_Date ?date ;" +
                "               :has_Owner ?ownerId ." +
                " ?ownerId      :has_Username ?username ." +
                "}";

        return  SPARQLservice.getResultsAsArray(queryString);
    }

    //----------------------------------------------------------

    public static void createEntrance(HashMap map) {
        String query = "INSERT DATA { " +
                ":" +
                UserService.getArrayList(map,"id") +
                " rdf:type owl:NamedIndividual ,\n" +
                "          :Entrance ;\n" +
                "       :has_ID \"" + UserService.getArrayList(map,"id") + "\"; \n" +
                "       :is_Main \"" + UserService.getArrayList(map,"Main") + "\"; \n" +
                "       :has_Orientation \"" + UserService.getArrayList(map,"Orientation") + "\"; \n" +
                "       :has_Size \"" + UserService.getArrayList(map,"Size") + "\"; \n" +
                "       :latitude \"" + UserService.getArrayList(map,"Latitude") + "\"; \n" +
                "       :longitude \"" + UserService.getArrayList(map,"Longitude") + "\". \n" +
                "}";
        SPARQLservice.insertData(query);
    }


    public static void createRoom(HashMap map) {
        String query = "INSERT DATA { " +
                ":" +
                UserService.getArrayList(map,"id") +
                " rdf:type owl:NamedIndividual ,\n" +
                "          :Room ;\n" +
                "       :has_ID \"" + UserService.getArrayList(map,"id") + "\"; \n" +
                "       :has_Description \"" + UserService.getArrayList(map,"Description") + "\"; \n" +
                "       :has_Name \"" + UserService.getArrayList(map,"Name") + "\"; \n" +
                "       :has_Depth \"" + UserService.getArrayList(map,"Depth") + "\"; \n" +
                "       :has_Usage \"" + UserService.getArrayList(map,"Usage") + "\"; \n" +
                "       :latitude \"" + UserService.getArrayList(map,"Latitude") + "\"; \n" +
                "       :longitude \"" + UserService.getArrayList(map,"Longitude") + "\". \n" +
                "}";
       SPARQLservice.insertData(query);
    }
    public static void createOrganism(HashMap map) {
        String query = "INSERT DATA { " +
                ":" +
                UserService.getArrayList(map,"id") + " rdf:type owl:NamedIndividual , :"+Cave.getIndividualType(map.get("Type").toString())+" ;\n" +
                " rdfs:subClassOf :Organism ; \n" +
                "       :has_ID \"" + UserService.getArrayList(map,"id") + "\"; \n" +
                "       :has_Name \"" + UserService.getArrayList(map,"Name") + "\"; \n" +
//                "       :has_Class \"" + UserService.getArrayList(map,"Class") + "\"; \n" +
                "       :has_Found_Date \"" + UserService.getArrayList(map,"Found Date") + "\"; \n" +
                "       :has_External_Source \"" + Cave.getExternalSource(UserService.getArrayList(map,"Type")) + "\" ; \n" +
                "       :has_Description \"" + UserService.getArrayList(map,"Description") + "\"; \n" +
                "       :has_Type \"" + Cave.getIndividualType(UserService.getArrayList(map,"Type")) + "\". \n" +
                "}";

        SPARQLservice.insertData(query);
    }

    public static void createRigging(HashMap map) {
        String query = "INSERT DATA { " +
                ":" +
                UserService.getArrayList(map,"id") +
                " rdf:type owl:NamedIndividual ,\n" +
                "          :Rigging ;\n" +
                "       :has_Description \"" + UserService.getArrayList(map,"Description") + "\"; \n" +
                "       :has_Date \"" + UserService.getArrayList(map,"Date") + "\"; \n" +
                "       :has_ID \"" + UserService.getArrayList(map,"id") + "\". \n" +
                "}";
       SPARQLservice.insertData(query);
    }

    public static void createSpeleothem(HashMap map) {
        String query = "INSERT DATA { " +
                ":" +
                UserService.getArrayList(map,"id") + " rdf:type owl:NamedIndividual , :"+Cave.getIndividualType(map.get("Type").toString())+" ;\n" +
                " rdfs:subClassOf :Speleothem ; \n" +
                "       :has_ID \"" + UserService.getArrayList(map,"id") + "\"; \n" +
                "       :has_External_Source \"" + Cave.getExternalSource(UserService.getArrayList(map,"Type")) + "\" ; \n" +
                "       :has_Type \"" + Cave.getIndividualType(UserService.getArrayList(map,"Type")) + "\"; \n" +
                "       :has_Description \"" + UserService.getArrayList(map,"Description") + "\"; \n" +
                "       :has_Usage \"" + UserService.getArrayList(map,"Usage") + "\"; \n" +
                "       :has_Estimated_Date \"" + UserService.getArrayList(map,"Estimated Date") + "\". \n" +
                "}";
       SPARQLservice.insertData(query);
    }

    public static void createExpedition(HashMap map) {
        String query = "INSERT DATA { " +
                ":" +
                UserService.getArrayList(map,"id") +
                " rdf:type owl:NamedIndividual ,\n" +
                "          :Expedition ;\n" +
                "       :has_ID \"" + UserService.getArrayList(map,"id") + "\"; \n" +
                "       :has_Participating_Club \"" + UserService.getArrayList(map,"Club") + "\"; \n" +
                "       :has_Start_Date \"" + UserService.getArrayList(map,"Start Date") + "\"; \n" +
                "       :has_End_Date \"" + UserService.getArrayList(map,"Finished Date") + "\"; \n" +
                "       :has_Leader \"" + UserService.getArrayList(map,"Leader") + "\"; \n" +
                "       :has_Informer \"" + UserService.getArrayList(map,"Informer") + "\"; \n" +
                "       :has_Description \"" + UserService.getArrayList(map,"Description") + "\"; \n" +
                "       :has_Type \"" + UserService.getArrayList(map,"Type") + "\". \n" +
                "}";
        SPARQLservice.insertData(query);
    }
    public static void createClimate(HashMap map) {
        String query = "INSERT DATA { " +
                ":" +
                map.get("id") +
                " rdf:type owl:NamedIndividual ,\n" +
                "          :Climate ;\n" +
                "       :has_ID \"" + map.get("id") + "\"; \n" +
                "       :has_Temperature \"" + map.get("Temperature") + "\"; \n" +
                "       :has_Humidity \"" + map.get("Humidity") + "\"; \n" +
                "       :has_Date \"" + map.get("Date") + "\"; \n" +
                "       :has_Air_Currents \"" + map.get("Air currents") + "\". \n" +
                "}";
        SPARQLservice.insertData(query);
    }

    public static void addEntrance(String entranceId,String id) {
        String query = "INSERT DATA {\n" +
                "        :" + id + " :has_Entrance :" + entranceId + " .\n" +
                "        }";
        SPARQLservice.insertData(query);
    }

    public static void addClimate(String climateId, String id) {
        String query = "INSERT DATA {\n" +
                "        :" + id + " :has_Climate :" + climateId + " .\n" +
                "        }";
        SPARQLservice.insertData(query);
    }
    public static void addRigging(String riggingId, String id) {
        String query = "INSERT DATA {\n" +
                "        :" + id + " :has_Rigging :" + riggingId + " .\n" +
                "        }";
        SPARQLservice.insertData(query);
    }

    public static void addRoom(String roomId, String id) {
        String query = "INSERT DATA {\n" +
                "        :" + id + " :has_Room :" + roomId + " .\n" +
                "        }";
        SPARQLservice.insertData(query);
    }

    public static void addSpeleothem(String speleothemId, String id) {
        String query = "INSERT DATA {\n" +
                "        :" + id + " :has_Speleothem :" + speleothemId + " .\n" +
                "        }";
        SPARQLservice.insertData(query);
    }
    public static void addOrganism(String organismId, String id) {
       String query = "INSERT DATA {\n" +
                "        :" + id + " :has_Organism :" + organismId + " .\n" +
                "        }";

        SPARQLservice.insertData(query);
    }
    public static void addExpedition(String expeditionId, String id) {
        String query = "INSERT DATA {\n" +
                "        :" + id + " :has_Expedition :" + expeditionId + " .\n" +
                "        }";
        SPARQLservice.insertData(query);
    }

    public static void addCaver(String caverId, String id) {
        String query = "INSERT DATA {\n" +
                "        :" + id + " :has_Caver :" + caverId + " .\n" +
                "        }";
        SPARQLservice.insertData(query);
    }

    public static void addCavingClub(String cavingClubId, String id) {
        System.out.println(cavingClubId);
        System.out.println(id);
        String query = "INSERT DATA {\n" +
                "        :" + id + " :has_Caving_Club :" + cavingClubId + " .\n" +
                "        }";
        SPARQLservice.insertData(query);
    }

    public static JsonArray getEntrances(String id) {
        String queryString = SPARQLservice.prefixes;
        queryString += "select *\n" +
                "where\n" +
                "{\n" +
                "     :" + id + " :has_Entrance ?entrance .\n" +
                "     ?entrance :has_ID ?id .\n" +
                "     ?entrance :is_Main ?main .\n" +
                "     ?entrance :has_Orientation ?orientation .\n" +
                "     ?entrance :has_Size ?size .\n" +
                "     ?entrance :latitude ?latitude .\n" +
                "     ?entrance :longitude ?Longitude .\n" +
                "}";
        return SPARQLservice.getResultsAsArray(queryString);
    }

    public static JsonArray getSpeleothems(String id) {

        String queryString = SPARQLservice.prefixes;
        queryString += "select *\n" +
                "where\n" +
                "{\n" +
                "     :"+id+" :has_Speleothem ?speleothem .\n" +
                "     ?speleothem :has_ID ?id .\n" +
                "     ?speleothem :has_Description ?description .\n" +
                "     ?speleothem :has_Type ?type .\n" +
                "     ?speleothem :has_Estimated_Date ?estimatedDate .\n" +
                "     ?speleothem :has_Usage ?usage .\n" +
                "     ?speleothem :has_External_Source ?externalSource .\n" +
                "\n" +
                "}";
        return SPARQLservice.getResultsAsArray(queryString);
    }

    public static JsonArray getOrganisms(String id) {
        String queryString = SPARQLservice.prefixes;
        queryString += "select *\n" +
                "where\n" +
                "{\n" +
                "     :"+id+" :has_Organism ?organism .\n" +
                "     ?organism :has_ID ?id .\n" +
                "     ?organism :has_Description ?description .\n" +
                "     ?organism :has_Name ?name .\n" +
//                "     ?organism :has_Class ?class .\n" +
                "     ?organism :has_Found_Date ?foundDate .\n" +
                "     ?organism :has_Type ?type .\n" +
                "     ?organism :has_External_Source ?externalSource .\n" +
                "\n" +
                "}";
        return SPARQLservice.getResultsAsArray(queryString);
    }

    public static JsonArray getClimate(String id) {
        String queryString = SPARQLservice.prefixes;
        queryString += "select *\n" +
                "where\n" +
                "{\n" +
                "     :"+id+" :has_Climate ?climate .\n" +
                "     ?climate :has_ID ?id .\n" +
                "     ?climate :has_Temperature ?temperature .\n" +
                "     ?climate :has_Air_Currents ?airCurrents .\n" +
                "     ?climate :has_Humidity ?humidity .\n" +
                "     ?climate :has_Date ?date .\n" +
                "}";
        return SPARQLservice.getResultsAsArray(queryString);
    }

    public static JsonArray getRooms(String id) {
        String queryString = SPARQLservice.prefixes;
        queryString += "select *" +
                "\n" +
                "where\n" +
                "{\n" +
                "     :"+id+" :has_Room ?room .\n" +
                "     ?room :has_ID ?id .\n" +
                "     ?room :has_Description ?description .\n" +
                "     ?room :has_Name ?name .\n" +
                "     ?room :latitude ?latitude .\n" +
                "     ?room :longitude ?longitude .\n" +
                "     ?room :has_Depth ?depth .\n" +
                "     ?room :has_Usage ?usage .\n" +
                "}";
        return SPARQLservice.getResultsAsArray(queryString);
    }

    public static JsonArray getExpeditions(String id) {
        String queryString = SPARQLservice.prefixes;
        queryString += "select *" +
                "\n" +
                "where\n" +
                "{\n" +
                "     :"+id+" :has_Expedition ?expedition .\n" +
                "     ?expedition :has_ID ?id .\n" +
                "     ?expedition :has_Description ?description .\n" +
                "     ?expedition :has_Type ?type .\n" +
                "     ?expedition :has_Participating_Club ?club .\n" +
                "     ?expedition :has_Start_Date ?startDate .\n" +
                "     ?expedition :has_End_Date ?finishedDate .\n" +
                "     ?expedition :has_Informer ?informer .\n" +
                "     ?expedition :has_Leader ?leader .\n" +
                "}";
        return SPARQLservice.getResultsAsArray(queryString);
    }

    public static JsonArray getRiggings(String id) {
        String queryString = SPARQLservice.prefixes;
        queryString += "select *\n" +
                "where\n" +
                "{\n" +
                "     :"+id+" :has_Rigging ?rigging .\n" +
                "     ?rigging :has_Description ?description .\n" +
                "     ?rigging :has_Date ?date .\n" +
                "     ?rigging :has_ID ?id .\n" +
                "}";
        return SPARQLservice.getResultsAsArray(queryString);
    }
    public static JsonArray getCavers(String id) {
        String queryString = SPARQLservice.prefixes;
        queryString += "select *\n" +
                "where\n" +
                "{\n" +
                "     :"+id+" :has_Caver ?caver .\n" +
                "     ?caver :has_ID ?id .\n" +
                "     ?caver :has_Username ?username .\n" +
                "     ?caver :has_First_Name ?firstName .\n" +
                "     ?caver :has_Last_Name ?lastName .\n" +
                "}";
        return SPARQLservice.getResultsAsArray(queryString);
    }

}
