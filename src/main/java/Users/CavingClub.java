package Users;

import Utilities.JSONservice;
import Utilities.SPARQLservice;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CavingClub {

    public static void addCavingClub(HashMap<String,ArrayList<String>> map) {

        StringBuilder query =
                new StringBuilder("INSERT DATA { " +
                        ":" + JSONservice.getString(map.get("id")) +
                        " rdf:type owl:NamedIndividual , :Caving_Club ;\n" +
                        " :has_Username \"" + JSONservice.getString(map.get("username")) + "\"; \n" +
                        " :has_Email \"" + JSONservice.getString(map.get("email")) + "\"; \n" +
                        " :has_Club_Name \"" + JSONservice.getString(map.get("clubName")) + "\"; \n" +
                        " :has_Web_Page \"" + JSONservice.getString(map.get("webpage")) + "\"; \n" +
                        " :has_Description \"" + JSONservice.getString(map.get("description")) + "\"; \n" +
                        " :has_Image \"" + JSONservice.getString(map.get("profilePhoto")) + "\"; \n" +
                        " :has_ID \"" + JSONservice.getString(map.get("id")) + "\"; \n");

        if (map.containsKey("admin")) {
            for (Object admin : map.get("admin").toArray()) {
                query.append(" :has_Admin " + ":").append(admin).append("; \n");
            }
        }

        query.append(" :founded_At \"").append(JSONservice.getString(map.get("foundedDate"))).append("\"; \n").append(" :has_Password \"").append(JSONservice.getString(map.get("password"))).append("\" .").append("}");

        SPARQLservice.insertData(query.toString());
        UserService.createEquipment(map.get("id").toString());
        UserService.createRiggingEquipment(map.get("id").toString());
    }

    public static JsonArray getCavingClubs() {
        String queryString = SPARQLservice.prefixes;
        queryString += "SELECT ?username ?Id \n";
        queryString += "WHERE { \n";
        queryString += "    ?s rdf:type owl:NamedIndividual, :Caving_Club ;" +
                " :has_ID ?Id ; \n" +
                " :has_Username ?username . \n";
        queryString += "}";
        return SPARQLservice.getResultsAsArray(queryString);
    }

    public static void setAdmin(HashMap map,String admin) {
        String query =
                "INSERT DATA { " +
                        ":" + admin +
                        " :is_Admin_At " + ":" + JSONservice.getString(map.get("id")) + "; \n" +
                        " :has_Participating_Club " + ":" + JSONservice.getString(map.get("id")) + ". \n" +
                        "}";

        SPARQLservice.insertData(query);
    }

    public static JsonObject getCavingClubGeneral(String id) {

        String queryString = SPARQLservice.prefixes;
        queryString += "SELECT ?username ?email ?webpage ?clubName ?description ?foundedDate ?admin ?profilePhoto\n";
        queryString += "WHERE { \n";
        queryString += "    ?s rdf:type owl:NamedIndividual ;" +
                " :has_ID \"" + id + "\" ; \n" +
                " :has_Email ?email ; \n" +
                " :has_Username ?username ; \n" +
                " :has_Web_Page ?webpage ; \n" +
                " :has_Club_Name ?clubName ; \n" +
                " :founded_At ?foundedDate ; \n" +
                " :has_Image ?profilePhoto ; \n" +
                " :has_Description ?description . \n" +
                " OPTIONAL { ?s :has_Admin ?admin }  \n";
        queryString += "}";

        JsonObject obj = SPARQLservice.getResults(queryString);
        obj.addProperty("numMembers", getNumMembers(id));

        if(obj.has("admin")){
            UserService.URI2Username(obj,"admin");
        }

        return obj;

    }

    public static String getNumMembers(String id) {

        String queryString = SPARQLservice.prefixes;
        queryString += " select (count(?caver) as ?numMembers) where\n" +
                "        {\n" +
                "        ?caver rdf:type owl:NamedIndividual, :Caver ;\n" +
                "                :has_Participating_Club " + ":" + id + ".\n" +
                "        }";

        return SPARQLservice.getResults(queryString).
                get("numMembers").getAsString();

    }

    public static JsonArray getMembers(String id) {
        String queryString = SPARQLservice.prefixes;
        queryString += "select *\n" +
                "where\n" +
                "{\n" +
                "     ?caver :has_Participating_Club :"+id+" .\n" +
                "     ?caver :has_ID ?id .\n" +
                "     ?caver :has_Username ?username .\n" +
                "     ?caver :has_First_Name ?firstName .\n" +
                "     ?caver :has_Last_Name ?lastName .\n" +
                "}";
        return SPARQLservice.getResultsAsArray(queryString);
    }
}