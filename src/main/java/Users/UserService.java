package Users;

import Utilities.JSONservice;
import Utilities.SPARQLservice;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;

public class UserService {


    public static boolean loginValidation(String username, String password)
    {
        String queryString = SPARQLservice.prefixes;

        queryString += "" +
            "ASK\n" +
            "{\n" +
            "  ?s :has_Username \"" + username + "\" ;\n" +
            "     :has_Password ?password ." +
            "  FILTER(?password = \"" + password + "\") .\n" +
            "}       \n" +
            "     ";

       return SPARQLservice.getAskResults(queryString);

    }

    public static JsonObject getUserByID(String id){

        String queryString = SPARQLservice.prefixes;
        queryString += "select ?username ?password \n" +
                "where\n" +
                "{\n" +
                "?s" +
                         " :has_ID \""+id+"\" . \n" +
                         " OPTIONAL { ?s :has_Password ?password }   \n" +
                         " OPTIONAL { ?s :has_Username ?username }  \n" +
                         " OPTIONAL { ?s rdf:type ?type }  \n" +

                "\n" +
                "}";

        return SPARQLservice.getResults(queryString);
    }

    public static JsonObject getUserByUsername(String username){
        String queryString = SPARQLservice.prefixes;
        queryString += "select ?id ?password ?type \n" +
                "where\n" +
                "{\n" +
                "?s" +
                " :has_Username \""+username+"\" . \n" +
                " OPTIONAL { ?s :has_Password ?password }   \n" +
                " OPTIONAL { ?s :has_ID ?id }  \n" +
                " OPTIONAL { ?s rdf:type ?type }  \n" +
                "\n" +
                "}";

        return SPARQLservice.getResults(queryString);
    }

    public static boolean emailExist(String email)
    {
        String queryString = SPARQLservice.prefixes;

        queryString += "" +
                "ASK\n" +
                "{\n" +
                " FILTER EXISTS { ?s :has_Email \"" + email + "\" }\n" +
                "}       \n" +
                "     ";

        return SPARQLservice.getAskResults(queryString);

    }

    public static boolean usernameExist(String username)
    {
        String queryString = SPARQLservice.prefixes;

        queryString += "" +
                "ASK\n" +
                "{\n" +
                " FILTER EXISTS { ?s :has_Username \"" + username + "\" }\n" +
                "}       \n" +
                "     ";

        return SPARQLservice.getAskResults(queryString);

    }

    public static boolean signUpValidation(HashMap map){
        return !usernameExist(JSONservice.getString(map.get("username"))) &&
               !emailExist(JSONservice.getString(map.get("email")));
    }


    public static void URI2Username(JsonObject obj,String param) {
        if (obj.get(param).isJsonArray()) {
            JsonArray arr2 = obj.get(param).getAsJsonArray();
            JsonArray temp = new JsonArray();
            for (JsonElement it2 : arr2) {
                temp.add(UserService.getUserByID(it2.getAsString().substring(it2.getAsString().lastIndexOf("#") + 1))
                        .get("username").getAsString());
            }
            obj.remove(param);
            obj.add(param,temp);
        }
        else {
            String admin = obj.get(param).getAsString();
            if (!(admin.equals("null"))) {
                obj.remove(param);
                obj.addProperty(param, UserService.getUserByID(admin.substring(admin.lastIndexOf("#") + 1))
                        .get("username").getAsString());
            }
        }
    }

    public static String getArrayList(HashMap map,String key){
        System.out.println(map.containsKey(key));
        if(map.containsKey(key)){
            return  map.get(key).toString().replace(",", "")
                    .replace("[", "")
                    .replace("]", "")
                    .trim();
        }
        return "";
    }

    public static void createEquipment(String userID){
        String query =
                "INSERT DATA { " +
                        " :Equipment_Collection_" + userID +
                        " rdf:type owl:NamedIndividual , :Equipment_Collection ;" +
                        " rdfs:subClassOf :Collection ." +
                        " :" + userID + " :has_Equipment :Equipment_Collection_"+ userID +" ." +
                        "}";
        SPARQLservice.insertData(query);
    }

    public static void createRiggingEquipment(String userID){
        String query =
                "INSERT DATA { " +
                        " :Rigging_Equipment_Collection_" + userID +
                        " rdf:type owl:NamedIndividual , :Rigging_Equipment_Collection ;" +
                        " rdfs:subClassOf :Collection ." +
                        " :" + userID + " :has_Rigging_Equipment :Rigging_Equipment_Collection_"+ userID +" ." +
                        "}";
        SPARQLservice.insertData(query);
    }

    public static String getEquimentType(String equipment_type){
        switch (equipment_type) {
            case "Backpacks":
                return "Backpacks";
            case "Cavepacks":
                return "Cavepacks";
            case "Electricity Generators":
                return "ElectricityGenerator";
            case "Lighting":
                return "Lighting";
            case "Measuring Instruments":
                return "MeasuringInstruments";
            case "Navigation Instruments":
                return "NavigationInstrument";
            case "Recording Tools":
                return "RecordingTools";
            case "Aluminum Blankets":
                return "AluminumBlanket";
            case "Helmets":
                return "Helmets";
            case "Drill Batteries":
                return "DrillBatteries";
            case "Drills":
                return "Drills";
            case "Dyneemas":
                return "Dyneemas";
            case "Plaques":
                return "Plaques";
            case "Rigging Sets":
                return "RiggingSets";
            case "Tapes":
                return "Tapes";
            case "Locking Carabiners":
                return "LockingCarabiners";
            case "Non Locking Carabiners":
                return "NonLockingCarabiners";
            default:
                return "other";
        }
    }

    public static void addEquipmentItem(HashMap map,String userID){
        String description = map.get("description").toString();
        description = description.replace("\n", "[newline]");
        String query =
                "INSERT DATA { " +
                        " :" + map.get("id") + " rdf:type owl:NamedIndividual , :" + getEquimentType(map.get("type").toString()) + " ;" +
                        " rdfs:subClassOf :Set_Of_Equipment , :Equipment ;" +
                        " :has_Description \"" + description + "\" ; \n" +
                        " :has_Quantity \"" + map.get("quantity") + "\" ; \n" +
                        " :has_Type \"" + getEquimentType(map.get("type").toString()) + "\" ; \n" +
                        " :has_ID \"" + map.get("id") + "\" . \n" +
                        " :Equipment_Collection_" + userID +" :SPLTHM_Equipment_Objects :" + map.get("id") + " ." +
                        "}";
        SPARQLservice.insertData(query);
    }

    public static JsonArray getEquipment(String id) {
        String queryString = SPARQLservice.prefixes;
        queryString += "select ?type ?description ?id ?quantity\n" +
                "where\n" +
                "{\n" +
                "     :"+id+" :has_Equipment ?equipment .\n" +
                "     ?equipment :SPLTHM_Equipment_Objects ?items .\n" +
                "     ?items :has_Type ?type .\n" +
                "     ?items :has_Description ?description .\n" +
                "     ?items :has_Quantity ?quantity .\n" +
                "     ?items :has_ID ?id .\n" +
                "\n" +
                "}";
        return SPARQLservice.getResultsAsArray(queryString);
    }

    public static void addRiggingEquipmentItem(HashMap map,String userID){
        String description = map.get("description").toString();
        description = description.replace("\n", "[newline]");
        String query =
                "INSERT DATA { " +
                        " :" + map.get("id") + " rdf:type owl:NamedIndividual , :" + getEquimentType(map.get("type").toString()) + " ;" +
                        " rdfs:subClassOf :Set_Of_Rigging_Equipment , :Rigging_Equipment ;" +
                        " :has_Description \"" + description + "\" ; \n" +
                        " :has_Quantity \"" + map.get("quantity") + "\" ; \n" +
                        " :has_Type \"" + getEquimentType(map.get("type").toString()) + "\" ; \n" +
                        " :has_ID \"" + map.get("id") + "\" . \n" +
                        " :Rigging_Equipment_Collection_" + userID +" :SPLTHM_Rigging_Equipment_Objects :" + map.get("id") + " ." +
                        "}";
        SPARQLservice.insertData(query);
    }

    public static JsonArray getRiggingEquipment(String id) {
        String queryString = SPARQLservice.prefixes;
        queryString += "select ?type ?description ?id ?quantity\n" +
                "where\n" +
                "{\n" +
                "     :"+id+" :has_Rigging_Equipment ?equipment .\n" +
                "     ?equipment :SPLTHM_Rigging_Equipment_Objects ?items .\n" +
                "     ?items :has_Type ?type .\n" +
                "     ?items :has_Description ?description .\n" +
                "     ?items :has_Quantity ?quantity .\n" +
                "     ?items :has_ID ?id .\n" +
                "\n" +
                "}";
        return SPARQLservice.getResultsAsArray(queryString);
    }

}
