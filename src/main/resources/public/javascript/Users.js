var User = {

    currentUser : undefined,
    currentTab : undefined ,
    visitedUser : undefined ,
    visitedUserId : undefined ,

    signUpInputs : {
        EMAIL : {id : 'email' , label : 'email' ,errorText: "not valid email", render : inputTextRender ,  value : getValue, validate: function () {
            var pattern = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
            if(this.value().match(pattern) === null){
                document.getElementById(this.id+'Error').style.display = 'block';
                return false;
            }
            document.getElementById(this.id+'Error').style.display = 'none';
            return true;
        } } ,
        USERNAME : {id : 'username' , label : 'username' ,errorText: "not valid username", render : inputTextRender , value : getValue, validate: function () {
            if(this.value().length < 3) {
                document.getElementById(this.id+'Error').style.display = 'block';
                return false;
            }
            document.getElementById(this.id+'Error').style.display = 'none';
            return true;
        } } ,
        PASSWORD : {id : 'password' , label : 'password' ,errorText: "not valid password", render : inputTextRender , value : getValue, validate: function () {
            if(this.value().length < 8){
                document.getElementById(this.id+'Error').style.display = 'block';
                return false;
            }
            document.getElementById(this.id+'Error').style.display = 'none';
            return true;
        }} ,
        CPASSWORD : {id : 'confirmPassword' , label : 'confirm password' , errorText: "no matching passwords",render : inputTextRender , value : getValue, validate: function () {
            if(this.value() !== document.getElementById("password").value){
                document.getElementById(this.id+'Error').style.display = 'block';
                return false;
            }
            document.getElementById(this.id+'Error').style.display = 'none';
            return true;
        } } ,
        FNAME : {id : 'firstName' , label : 'first name' ,errorText: "necessary field", render : inputTextRender , value : getValue, validate: function () {
            if(this.value() === ""){
                document.getElementById(this.id+'Error').style.display = 'block';
                return false;
            }
            document.getElementById(this.id+'Error').style.display = 'none';
            return true;
        } } ,
        LNAME : {id : 'lastName' , label : 'last name' ,errorText: "necessary field", render : inputTextRender , value : getValue, validate: function () {
            if(this.value() === ""){
                document.getElementById(this.id+'Error').style.display = 'block';
                return false;
            }
            document.getElementById(this.id+'Error').style.display = 'none';
            return true;
        } } ,
        GENDER : {id : 'gender' , label : 'gender' , render : inputRadioRender , value : getValue, validate: function () {
            return true;
        } } ,
        RESQUE : {id : 'resquePartitipation' , label : 'resque participation' , render : inputRadioRender , value : getValue, validate: function () {
            return true;
        } } ,
        PCLUB : { id : 'participatingClub' ,label : 'participating club' , render : inputTextRender , value : getParticipatingClub, validate: function () {
            if(autocompleteSrc.includes(document.getElementById("participatingClub").value) ||
                document.getElementById("participatingClub").value === "")
            {
                return true;
            }
            alert("participating club not exists");
            return false;
        } } ,
        PHOTO : {id : 'profilePhoto' , label : 'profile photo' , render : uploadButtonRender, value : function () {
        } , validate: function () {
            return true;
        } } ,
        CNAME : {id : 'clubName' , label : 'club name' ,errorText: "necessary field", render : inputTextRender , value : getValue , validate: function () {
            if(this.value() === ""){
                document.getElementById(this.id+'Error').style.display = 'block';
                return false;
            }
            document.getElementById(this.id+'Error').style.display = 'none';
            return true;
        }  } ,
        FDATE : {id : 'foundedDate' , label : 'founded date' , render : inputTextRenderDatePicker , value : getDate , validate: function () {
            return true;
        }  } ,
        WEBPAGE : {id : 'webpage' , label : 'webpage' , render : inputTextRender , value : getValue , validate: function () {
            return true;
        }  } ,
        DESCRIPTION : {id : 'description' , label : 'Description' , render : textAreaRender , value : getValue , validate: function () {
            return true;
        }  } ,
        ADMIN : {id : 'admin' , label : 'admin' , render : inputMultipleRender , value : getAdmin , validate: function () {
            var values = $(":input[type=text][readonly='readonly']").val("");
            alert(values);
            if(values.length === 0){
                alert("admin is neccessary");
                return false;
            }
            return true;
        }  } ,
        NAME : {id : 'name' , label : 'Name' , render : inputTextRender , value : getValue , validate : function (){
            return true;
        } },
        CATNAME : {id : 'catalogueName' , label : 'Catalogue Name' , render : inputTextRender , value : getValue , validate : function (){
            return true;
        } }
        ,
        LONAME : {id : 'localName' , label : 'Local Name' , render : inputTextRender , value : getValue , validate : function (){
            return true;
        } },
        ONAME : {id : 'otherName' , label : 'Other Name' , render : inputTextRender , value : getValue , validate : function (){
            return true;
        } },
        CTYPE : {id : 'caveType' , label : 'Cave Type' , render : inputTextRender , value : getValue , validate : function (){
            return true;
        } },
        DEPTH : {id : 'depth' , label : 'Depth' , render : inputTextRender , value : getValue , validate : function (){
            return true;
        } },
        ALTITUDE : {id : 'altitude' , label : 'Altitude' , render : inputTextRender , value : getValue , validate : function (){
            return true;
        } },
        LENGTH : {id : 'length' , label : 'Length' , render : inputTextRender , value : getValue , validate : function (){
            return true;
        } },
        KNOWNDATE : {id : 'knownFrom' , label : 'Known From' , render : inputTextRenderDatePicker , value : getValue , validate : function (){
            return true;
        } },
        VISITABLE : {id : 'visitable' , label : 'Visitable' , render : inputRadioRender , value : getValue , validate : function (){
            return true;
        } },
        USAGE : {id : 'usage' , label : 'Usage' , render : inputTextRender , value : getValue , validate : function (){
            return true;
        } },
        EDATE : {id : 'estimatedUsageDate' , label : 'Estimated Usage Date' , render : inputTextRenderDatePicker , value : getValue , validate : function (){
            return true;
        } },
        LOCATION : {id : 'location' , label : 'Add ress' , render : inputTextRender , value : getValue , validate : function (){
            return true;
        } },
        LAT : {id : 'latitude' , label : 'Latitude' , render : inputTextRender , value : getValue , validate : function (){
            return true;
        } },
        LONG : {id : 'longtitude' , label : 'Longtitude' , render : inputTextRender , value : getValue , validate : function (){
            return true;
        } },
        PROVINCE : {id : 'province' , label : 'Province' , render : inputTextRender , value : getValue , validate : function (){
            return true;
        } },
        ADREGION : {id : 'administrativeRegion' , label : 'Administrative Region' , render : inputTextRender , value : getValue , validate : function (){
            return true;
        } },
        CITY : {id : 'city' , label : 'City' , render : inputTextRender , value : getValue , validate : function (){
            return true;
        } },
        COUNTRY : {id : 'country' , label : 'Country' , render : inputTextRender , value : getValue , validate : function (){
            return true;
        } },
        MUNICIPALITY : {id : 'municipality' , label : 'Municipality' , render : inputTextRender , value : getValue , validate : function (){
            return true;
        } },
        SETTLEMENT : {id : 'settlement' , label : 'Settlement' , render : inputTextRender , value : getValue , validate : function (){
            return true;
        } }
    } ,

    create : function (state) {
        this.currentUser = state;
    } ,

    validation : function () {
        for(var i in this.currentUser.inputs){

            var input = User.currentUser.inputs[i];

            var a = User.signUpInputs[input].validate();

            if(a === false){
                return false;
            }
        }
        return true;
    } ,

    signUp : function () {

        var inp = {};

        for(var i in this.currentUser.inputs){
            var input = User.currentUser.inputs[i];
            if(input !== "ADMIN"){
                inp[User.signUpInputs[input].id] =
                    User.signUpInputs[input].value();
            }
            else {
               inp["admin"] = getAdmin("adminInputs");
            }
        }

        if(User.validation(this.currentUser)){
            var ID = Utilities.idGenerator(this.currentUser.prefix);
            inp['id'] = ID;
            var src = Sketch.upload(ID);

            inp['profilePhoto'] = src;
            var path;
            if(User.currentUser.name === "Caves")
                path = '/api/'+this.currentUser.name+'/SignUp';
            else
                path = '/api/UserProfiles/'+this.currentUser.name+'/SignUp';

            Utilities.ajaxCall(inp,path,'POST').done(function(data){
                if(data === "true" && (User.currentUser.name === "Cavers" ||
                    User.currentUser.name === "CavingClubs"))
                        view.change(view.homePageView);
                else if(data === "true" && User.currentUser.name === "Caves"){
                    User.visitedUser = User.Cave;
                    User.visitedUserId = inp['id'];
                    if(Utilities.cookieValue("id").substring(0,2) === "CC")
                        User.currentUser = User.cavingClub;
                    else if(Utilities.cookieValue("id").substring(0,2) === "CA")
                        User.currentUser = User.Cavers;
                    view.change(view.profilePageView);
                }
                else
                    alert("not valid given email or username");

            });

        }

    } ,

    signUpRender : function () {
        var i = 0;
        for(var s in User.currentUser.inputs)
        {
            var input = User.currentUser.inputs[s];

            document.getElementById('signUpForm').innerHTML += '' +
                '<div class="form-group">' +
                '<label class="control-label col-sm-3">'+User.signUpInputs[input].label+'<span class="text-danger">*</span></label>' +
                '<div class="col-md-8 col-sm-9" id="inputsField'+i+'">' +
                // adding inputs here
                '</div>' +
                '</div>';

            if(input === 'GENDER')
                User.signUpInputs[input].render(User.signUpInputs[input],i,['male','female']);
            else if(input === 'RESQUE' || input === 'VISITABLE')
                User.signUpInputs[input].render(User.signUpInputs[input],i,['yes','no']);
            else if(input === 'PHOTO')
                User.signUpInputs[input].render(i);
            else
                User.signUpInputs[input].render(User.signUpInputs[input],i);

            i++;
        }

        document.getElementById('signUpForm').innerHTML += '' +
            ' <div class="form-group">' +
            '<div class="col-xs-offset-3 col-xs-10">' +
            '<button id="signUpBtn" class="btn btn-primary">Sign Up</button>' +
            '</div>' +
            '</div>';

        // sending inputs values to DB
        document.getElementById("signUpBtn").addEventListener("click",function () {
            User.signUp();
        });

    } ,

    logout : function () {
        document.cookie = "id=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        location.reload();
    } ,

    login : function () {
        var inp = {};
        inp["username"] = document.getElementById("loginUsername").value;
        inp["password"] = document.getElementById("loginPassword").value;

        loginAjaxCall(inp).done(function(data){

            var obj = JSON.parse(data);
            if(obj.type[0] === "http://www.speleothem.org/vocabulary/alpha/speleothem#Caver" ||
               obj.type[1] === "http://www.speleothem.org/vocabulary/alpha/speleothem#Caver" ){
                document.cookie = "id="+obj.id;
                User.currentUser = User.Cavers;
                User.visitedUser = User.Cavers;
                User.visitedUserId = Utilities.cookieValue("id");
                navBar.change(navBar.LoggedIn);
                view.change(view.profilePageView);
            }
            else if(obj.type[0] === "http://www.speleothem.org/vocabulary/alpha/speleothem#Caving_Club" ||
                   obj.type[1] === "http://www.speleothem.org/vocabulary/alpha/speleothem#Caving_Club"){
                document.cookie = "id="+obj.id;
                User.currentUser = User.cavingClub;
                User.visitedUser = User.cavingClub;
                User.visitedUserId = Utilities.cookieValue("id");
                navBar.change(navBar.LoggedIn);
                view.change(view.profilePageView);
            }
            else {
                alert("not valid username or password");
            }
        });

    } ,

    tabs : {
        GENERAL : { id : "general" , label : "General" , render : function () {User.generalContent.renderGeneral();}} ,
        EXPEDITIONS : { id : "expeditions" , label : "Expeditions" , render : function () {Table.change(Table.EXPEDITIONS,false)}} ,
        MEMBERS : { id : "members" , label : "Members" , render : function () {Table.change(Table.MEMBERS,false)}} ,
        EQUIPMENT : { id : "equipment" , label : "Equipment" , render : function () {Table.change(Table.EQUIPMENT,false)}} ,
        RIGGINGEQ: {id : "riggingEquipment" , label : "Rigging equipment" , render : function () {Table.change(Table.RIGGINGEQ,false)}} ,
        PHOTOS: {id : "photo" , label : "Photo" , render : function () {Table.change(Table.PHOTOS,false)}} ,
        SKETCHES: {id : "sketch" ,label : "Sketch" , render : function () {Table.change(Table.SKETCHES,false)}} ,
        LOGBOOK: {id : "logbook" ,label : "Logbook" , render : function () {Table.change(Table.LOGBOOK,false)}} ,
        ENTRANCES: {id : "entrances" ,label : "Entrances" , render : function () {Table.change(Table.ENTRANCES,false)}} ,
        ROOMS: {id : "rooms" ,label : "Rooms" , render : function () {Table.change(Table.ROOMS,false)}} ,
        CAVERS: {id : "cavers" ,label : "Cavers" , render : function () {Table.change(Table.CAVERS,false)}} ,
        CLIMATE: {id : "climate" ,label : "Climate" , render : function () {Table.change(Table.CLIMATE,false)}} ,
        SPELEOTHEMS: {id : "speleothems" ,label : "Speleothems" , render : function () {Table.change(Table.SPELEOTHEMS,false)}} ,
        ORGANISMS: {id : "organisms" ,label : "Organisms" , render : function () {Table.change(Table.ORGANISMS,false)}} ,
        RIGGINGS: {id : "riggings" ,label : "Riggings" , render : function () {Table.change(Table.RIGGINGS,false)}}
    } ,

    generalContent : {
        init : function (obj) {
            document.getElementById("generalTabContent").innerHTML += '' +
                '<br>' +
                '<div class="container">'+
                '<div class="row" id="panelArea">' +
                '</div>' +
                '</div>';
        } ,
        createPanel : function (panelID,heading) {
            document.getElementById("panelArea").innerHTML += '' +
                '<div class="col-sm-9">' +
                '<div class="panel panel-default" id="'+panelID+'">'+
                '<div class="panel-heading"><h4>'+heading+'</h4></div>' +
                '</div>' +
                '</div>';

        } ,
        createProfileImg : function (obj) {

            var image;

            if(obj.profilePhoto === undefined || obj.profilePhoto === "null"){
                image = '<img class="img-circle" src = "../Files/default-profile-picture.png" width="200" height="200" >';
            }
            else{
                // new Modal().create();
                // document.getElementById('ModalBody').innerHTML = '<img src="'+obj.profilePhoto+'" class="img-responsive">';
                // data-toggle="modal" data-target="#emptyModal"
                // image = '<img src= "localhost:4567/'+obj.profilePhoto+'" width="200" height="200" class="img-circle">';
                image = '<img class="img-circle" src = "../Files/default-profile-picture.png" width="200" height="200" >';
            }


            document.getElementById("panelArea").innerHTML += '' +
                '<div class="panel panel-default" id="panelImage">'+
                '<div class="panel-body text-center">' +
                // '<div class="pull-left">'+
                    image +
                '<h3 id="profileTitle"></h3>'+
                // '</div>' +
                '</div>'+
                '</div>';

            if(User.visitedUser.name === "Caves")
                document.getElementById("profileTitle").innerHTML = obj.name;
            else
                document.getElementById("profileTitle").innerHTML = obj.username;
        } ,
        createListGroups: function (obj,info) {

            var fields;
            if(info === "other_info")
                fields = User.visitedUser.generalFields.other_info;
            else if(info === "personal_info")
                fields = User.visitedUser.generalFields.personal_info;
            else if(info === "links")
                fields = User.visitedUser.generalFields.links;


            var listGroup  = '<ul class="list-group">';
            for(var i in fields)
            {
                if(!(obj[i] === undefined))
                {
                    if(fields === User.visitedUser.generalFields.links){
                        if(obj[fields[i].ref] !== "")
                        listGroup += '<li class="list-group-item"><span><strong>'+fields[i].label+': ' +
                            '</strong></span><span id="'+i+'"> <a href="'+obj[fields[i].ref]+'">'+obj[i]+'</a></span></li>';
                        else
                            listGroup += '<li class="list-group-item"><span><strong>'+fields[i].label+': ' +
                                '</strong></span><span id="'+i+'">'+obj[i]+'</span></li>';
                    }
                    else {
                        listGroup += '<li class="list-group-item"><span><strong>'+fields[i]+': ' +
                            '</strong></span><span id="'+i+'"> '+obj[i]+'</span></li>';
                    }

                }

            }
            return listGroup;
        } ,
        renderGeneral : function () {
            var path;
            if(User.visitedUser.name === "Caves")
                path = '/api/'+User.visitedUser.name+'/'+User.visitedUserId+'/general';
            else
                path = '/api/UserProfiles/'+User.visitedUser.name+'/'+User.visitedUserId+'/general';

            Utilities.ajaxCallGET(path).done(function(data){

                var obj = JSON.parse(data);
                User.generalContent.init(obj);
                User.generalContent.createProfileImg(obj);
                document.getElementById("panelArea").innerHTML += '' +
                    '<div class="col-md-3" id="groupLists">' +
                    '</div>';

                document.getElementById("groupLists").innerHTML += User.generalContent.createListGroups(obj,"links") ;
                document.getElementById("groupLists").innerHTML += User.generalContent.createListGroups(obj,"other_info") ;

            if((User.visitedUser.name === "CavingClubs") ||(User.visitedUser.name === "Caves") )
                User.generalContent.createPanel("descriptionInfo","Description") ;

            if((obj['description'] !== undefined))
                    document.getElementById("descriptionInfo").innerHTML += '<div class="panel-body"><p> '+obj.description+'</p></div>';

                User.generalContent.createPanel("personalInfo","Personal Information");
                document.getElementById("personalInfo").innerHTML += User.generalContent.createListGroups(obj,"personal_info") ;

            });
        }
    } ,

    profileRender : function(){
        for(var i in User.visitedUser.profileTabs) {

            var profileTab = this.visitedUser.profileTabs[i];
            if(this.visitedUser.profileTabs[i] === "GENERAL"){
                document.getElementById("tabs").innerHTML += '' +
                    '<li class=" active" id="general"><a data-toggle="tab" onclick="preventTab(event)"  href="#generalTabContent">General</a></li>';
                document.getElementById("tabContent").innerHTML += '' +
                    '<div id="generalTabContent" class="tab-pane fade in active" ></div>';
                User.generalContent.renderGeneral();
                User.currentTab = User.tabs.GENERAL.id;
            }else {
                document.getElementById("tabs").innerHTML += '' +
                    '<li id="' + this.tabs[profileTab].id + '"><a data-toggle="tab" onclick="preventTab(event)" href="#' + this.tabs[profileTab].id + 'TabContent">' + this.tabs[profileTab].label + '</a></li>';
                document.getElementById("tabContent").innerHTML += '' +
                    '<div id="' + this.tabs[profileTab].id + 'TabContent" class="tab-pane fade in" ></div>';
            }
        }

        for(var i in this.visitedUser.profileTabs) {

            var _profileTab = this.visitedUser.profileTabs[i];
            if(this.visitedUser.profileTabs[i] !== "GENERAL") {
                document.getElementById(this.tabs[_profileTab].id).onclick = (function (j) {
                    return function () {
                        User.currentTab = j;
                        User.tabs[j].render();
                        return false;
                    }
                })(_profileTab);
            }
            else{
                document.getElementById(this.tabs[_profileTab].id).onclick = (function (j) {
                    return function () {
                        User.currentTab = j;
                        return false;
                    }
                })(_profileTab);
            }
        }
    } ,

    Cavers : {

        privileges : 'LIMITED',
        prefix : 'CA' ,
        name : "Cavers" ,

        inputs : [
            "EMAIL" , "USERNAME" , "PASSWORD" , "CPASSWORD" ,
            "FNAME" , "LNAME" , "GENDER" , "RESQUE" ,
            "PCLUB" ,"PHOTO"
        ] ,

        autocomplete : function () {

            usernameToId = {};
            autocompleteSrc = [];

            Utilities.ajaxCallGET('/api/UserProfiles/CavingClubs').done(function(data){
                var obj = JSON.parse(data);
                for(var i in obj){
                    usernameToId[obj[i].username] = obj[i].Id;
                }
                for(var j in usernameToId){
                    autocompleteSrc.push(j);
                }
                $("#participatingClub").autocomplete({
                    source: autocompleteSrc
                });
            });

        } ,

        profileTabs : [
            "GENERAL" , "EXPEDITIONS" , "EQUIPMENT" , "RIGGINGEQ" ,
            "PHOTOS" , "SKETCHES" , "LOGBOOK"
        ] ,

        generalFields : {
            other_info: {
              partClub : "Participating club" ,
              admin : "Admin" ,
              resque : "Resque"
            } ,
            personal_info : {
                firstName : "First Name" ,
                lastName : "Last Name" ,
                username : "Username" ,
                gender : "Gender" ,
                email : "Email"
            }
        }
    },

    cavingClub : {

        privileges : 'NON-LIMITED',
        prefix : 'CC' ,
        name : "CavingClubs" ,

        inputs : [
            "EMAIL" , "USERNAME" , "PASSWORD" , "CPASSWORD" ,
            "CNAME" , "FDATE" , "WEBPAGE" , "DESCRIPTION" ,
            "ADMIN" , "PHOTO"
        ] ,

        autocomplete : function () {

            usernameToId = {};
            autocompleteSrc = [];

            Utilities.ajaxCallGET('/api/UserProfiles/Cavers').done(function(data){
                var obj = JSON.parse(data);
                for(var i in obj){
                    usernameToId[obj[i].username] = obj[i].Id;
                }
                for(var j in usernameToId){
                    autocompleteSrc.push(j);
                }
                $(".adminInputs").autocomplete({
                    source: autocompleteSrc
                });
            });
        } ,

        profileTabs : [
            "GENERAL" , "EXPEDITIONS" , "MEMBERS" , "EQUIPMENT" ,
            "RIGGINGEQ" , "PHOTOS" , "SKETCHES"
        ] ,

        generalFields : {
            other_info: {
                foundedDate : "Founded Date" ,
                numMembers : "Members" ,
                admin : "Admin"
            } ,
            personal_info : {
                clubName : "Club Name" ,
                username : "Username" ,
                webpage : "Webpage" ,
                email : "Email"
            } ,
            description : "description"
        }
    } ,

    Cave : {

        privileges : undefined,
        prefix : 'CV' ,
        name : "Caves" ,

        inputs : [
            "DESCRIPTION" , "NAME" , "CATNAME" , "LONAME" ,
            "ONAME" , "CTYPE" , "DEPTH" , "ALTITUDE" ,
            "LENGTH" , "KNOWNDATE" , "VISITABLE" ,
            "USAGE" , "EDATE" , "LOCATION" ,"LAT" , "LONG",
            "CITY" , "ADREGION" , "PROVINCE"  ,
            "MUNICIPALITY" , "SETTLEMENT" , "COUNTRY" , "PHOTO"
        ] ,

        autocomplete : function () {

            var type = ["ice cave" , "karst cave" , "sea cave" , "show cave" , "wild cave"];
            var usage = ["food conservation" , "ritual" , "refuge"];

            $("#usage").autocomplete({
                minLength: 0,
                source: usage
            }).focus(function() {
                $(this).autocomplete("search", "");
            });

            $("#caveType").autocomplete({
                minLength: 0,
                source: type
            }).focus(function() {
                $(this).autocomplete("search", "");
            });

            var placesAutocomplete = places({
                container: document.querySelector('#location'),
                type: 'address',
                templates: {
                    value: function(suggestion) {
                        return suggestion.name;
                    }
                }
            });

            var placesAutocomplete2 = places({
                container: document.querySelector('#city'),
                type: 'city',
                templates: {
                    value: function(suggestion) {
                        return suggestion.name;
                    }
                }
            });

            document.getElementById("location").onchange = function () {
                document.querySelector('#administrativeRegion').value = '';
                document.querySelector('#city').value = '';
                document.querySelector('#province').value = '';
                document.querySelector('#municipality').value = '';
                document.querySelector('#country').value = '';
                document.querySelector('#latitude').value = '';
                document.querySelector('#longtitude').value = '';
            };

            document.getElementById("city").onchange = function () {
                document.querySelector('#administrativeRegion').value = '';
                document.querySelector('#province').value = '';
                document.querySelector('#municipality').value = '';
                document.querySelector('#country').value = '';
            };

            placesAutocomplete.on('change', function resultSelected(e) {
                document.querySelector('#administrativeRegion').value = e.suggestion.administrative || '';
                document.querySelector('#city').value = e.suggestion.city || '';
                document.querySelector('#province').value = e.suggestion.county || '';
                document.querySelector('#municipality').value = e.suggestion.county || '';
                document.querySelector('#country').value = e.suggestion.country || '';
                document.querySelector('#latitude').value = e.suggestion.latlng.lat || '';
                document.querySelector('#longtitude').value = e.suggestion.latlng.lng || '';
            });

            placesAutocomplete2.on('change', function resultSelected(e) {
                document.querySelector('#administrativeRegion').value = e.suggestion.administrative || '';
                document.querySelector('#province').value = e.suggestion.county || '';
                document.querySelector('#municipality').value = e.suggestion.county || '';
                document.querySelector('#country').value = e.suggestion.country || '';
            });
        } ,

        profileTabs : [
            "GENERAL" , "ENTRANCES" , "ROOMS" , "EXPEDITIONS" ,
            "CAVERS" , "CLIMATE" , "SPELEOTHEMS" , "ORGANISMS"
            , "RIGGINGS" , "PHOTOS" , "SKETCHES"
        ] ,

        generalFields : {
            links : {
                caveType : {label : "Cave Type" , ref : "externalSource" }
            },
            other_info: {
                length : "Length" ,
                depth : "Depth" ,
                altitude : "Altitude" ,
                latitude : "Lat",
                longitude : "Long",
                knownFrom : "Known From",
                visitable : "Visitable"
            } ,
            personal_info : {
                name : "Name" ,
                catalogueName : "Catalogue Name" ,
                localName : "Local Name" ,
                otherName : "Other Name" ,
                location : "Address" ,
                province : "Province" ,
                administrativeRegion : "Administrative Region" ,
                city : "City" ,
                country : "Country" ,
                municipality : "Municipality" ,
                settlement : "Settlement"
            } ,
            description : "description"
        }
    }

};

function loginAjaxCall(data) {
    return $.ajax({
        method: 'POST',
        url: '/api/UserProfiles/Login',
        data: JSON.stringify(data),
        success: function (data) {
        }
    });
}

function preventTab(event) {
        if(Table.currentTable.editModeId !== undefined){
            alert("Unsaved changes");
            event.preventDefault();
            event.stopPropagation();
            return false;
        }
}