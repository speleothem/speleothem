var Utilities =  {

    idGenerator : function (prefix) {
       var id = prefix + Math.random().toString(16).slice(2);
       return id;
    } ,

    ajaxCall : function (data,path,method) {
        return $.ajax({
            method: method,
            url: path,
            data: JSON.stringify(data),
            success: function (data) {
            }
        });
    } ,

    ajaxCallGET : function (path) {
        return $.ajax({
            method: 'GET' ,
            async: false ,
            url: path,
            success: function (data) {
            }
        });
    } ,

    cookieValue : function(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length === 2) return parts.pop().split(";").shift();
}
};