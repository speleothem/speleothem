var Image = {

    currentCategory : "inside" ,

    handleCategories : function(category,btn){
        $(".photoButtons").removeClass("active");
        $(btn).addClass("active");
        Image.currentCategory = btn.getAttribute("id").replace("Button","");
        var i;
        var categories = document.getElementsByClassName("categories");
        for(i = 0; i < 4; i++){
            categories[i].style.display = "none";
        }
        document.getElementById(category).style.display = "block";
    },


    renderButtons : function(){

        return '<div id="photoButtons" style="margin-bottom: 11px;">' +
            ' <h3>Photos</h3>' +
            ' <button type="button" id="insideButton" class="btn btn-primary photoButtons active" onclick="Image.handleCategories(\'insideCategory\',this)">Inside</button>' +
            ' <button type="button" id="outButton" class="btn btn-primary photoButtons" onclick="Image.handleCategories(\'outCategory\',this)">Out</button>' +
            ' <button type="button" id="speleothemsButton" class="btn btn-primary photoButtons" onclick="Image.handleCategories(\'speleothemsCategory\',this)">Speleothems</button>' +
            ' <button type="button" id="organismsButton" class="btn btn-primary photoButtons" onclick="Image.handleCategories(\'organismsCategory\',this)">Organisms</button>' +
            '<div class="upload-btn-wrapper pull-right">'+
            '<button id="uploadBtn">Upload a file</button>'+
            '<input type="file" class="imgur" name="myfile" accept="image/*" onchange="testImage();"/>'+
            '</div>'+
            '</div>';

    },

    render : function (modalMode,id,targetId) {
        var toAdd;
        var tableId;

        if(id === undefined)
            tableId = Table.currentTable.id;
        else{
            tableId = id;
        }

        if(modalMode === undefined) {

            toAdd = document.getElementById("photoTabContent");
            toAdd.innerHTML = '' +
                Image.renderButtons()+
                ' <div class="row">' +
                ' <div class="container">' +
                ' <div id="insideCategory" class="categories">' +
                ' </div>' +
                ' <div id="speleothemsCategory" class="categories" style="display: none">' +
                ' </div>' +
                ' <div id="outCategory" class="categories" style="display: none">' +
                ' </div>' +
                ' <div id="organismsCategory" class="categories" style="display: none">' +
                ' </div>' +
                ' </div>' +
                '</div>';

            if(Table.currentTable.id === "photos"){
                document.getElementById("uploadBtn").style.display = 'none';
            }

            $.ajax({
                type: 'GET',
                url: "/api/Caves/"+User.visitedUserId+"/photos",
                processData: false,
                contentType: false,
                async: false
            }).done(function(data) {
                var obj = JSON.parse(data);
                for(var i = 0; i < obj.length; i++)
                {

                    document.getElementById(obj[i].imageType+"Category").innerHTML += ' <div class="col-lg-3 col-sm-4 col-6">' +
                        '<div class="thumbnail">' +
                        '<img ' +
                        'src="'+obj[i].link+'" class="thumbnail img-responsive wh">'+
                        '<div class="caption">'+
                        '<p>Uploaded by : '+obj[i].username+'<br>Date: '+obj[i].date+'</p>'+
                   '</div>'+
                   '</div>';
                }
            });
        }
        else{

            var c = Table.counter - 1 ;
            if(Table.counter ===  1){
                document.getElementById("toAddIndi").innerHTML += '<li data-target="#' + targetId + 'Modal" data-slide-to="'+c+'" class="active"></li>';
                toAdd = document.getElementById("toAddItem");
                toAdd.innerHTML += '' +
                    '<div class="item active">' +
                    Image.renderButtons() +
                    ' <div class="row">' +
                    ' <div class="container">' +
                    ' <div id="insideCategory" class="categories">' +
                    ' </div>' +
                    ' <div id="speleothemsCategory" class="categories" style="display: none">' +
                    ' </div>' +
                    ' <div id="outCategory" class="categories" style="display: none">' +
                    ' </div>' +
                    ' <div id="organismsCategory" class="categories" style="display: none">' +
                    ' </div>' +
                    ' </div>' +
                    '</div>'+
                    '</div>';


                $.ajax({
                    type: 'GET',
                    url: "/api/Caves/"+User.visitedUserId+"/"+Table.tableStack[Table.counter - 1].id+"/"+Table.rowStack[Table.counter - 1]+"/"+Table.currentTable.id,
                    processData: false,
                    contentType: false,
                    async: false
                }).done(function(data) {
                    var obj = JSON.parse(data);
                    for(var i = 0; i < obj.length; i++)
                    {
                        document.getElementById(obj[i].imageType+"Category").innerHTML += ' <div class="col-lg-3 col-sm-4 col-6">' +
                            '<div class="thumbnail">' +
                            '<img ' +
                            'src="'+obj[i].link+'" class="thumbnail img-responsive wh">'+
                            '<div class="caption">'+
                            '<p>Uploaded by : '+obj[i].username+'<br>Date: '+obj[i].date+'</p>'+
                            '</div>'+
                            '</div>';
                    }
                });

            }
            else if(Table.counter >=  1){

                document.getElementById("toAddIndi").innerHTML += '<li id="trigger" data-target="#' + targetId + 'Modal" data-slide-to="'+c+'"></li>';
                toAdd = document.getElementById("toAddItem");
                toAdd.innerHTML += '' +
                    '<div class="item">' +
                    Image.renderButtons() +
                    ' <div class="row">' +
                    ' <div class="container">' +
                    ' <div id="insideCategory" class="categories">' +
                    ' </div>' +
                    ' <div id="speleothemsCategory" class="categories" style="display: none">' +
                    ' </div>' +
                    ' <div id="outCategory" class="categories" style="display: none">' +
                    ' </div>' +
                    ' <div id="organismsCategory" class="categories" style="display: none">' +
                    ' </div>' +
                    ' </div>' +
                    '</div>'+
                    '</div>';

                document.getElementById("uploadBtn").style.display = 'none';

                $.ajax({
                    type: 'GET',
                    url: "/api/Caves/"+User.visitedUserId+"/"+Table.tableStack[Table.counter - 1].id+"/"+Table.rowStack[Table.counter - 1]+"/"+Table.currentTable.id,
                    processData: false,
                    contentType: false,
                    async: false
                }).done(function(data) {
                    var obj = JSON.parse(data);
                    for(var i = 0; i < obj.length; i++)
                    {
                        document.getElementById(obj[i].imageType+"Category").innerHTML += ' <div class="col-lg-3 col-sm-4 col-6">' +
                            '<div class="thumbnail">' +
                            '<img ' +
                            'src="'+obj[i].link+'" class="thumbnail img-responsive wh">'+
                            '<div class="caption">'+
                            '<p>Uploaded by : '+obj[i].username+'<br>Date: '+obj[i].date+'</p>'+
                            '</div>'+
                            '</div>';
                    }
                });

                $('#trigger').click();
                document.getElementById("trigger").removeAttribute("id");

            }

        }


    } ,

    upload : function (ID) {
        // upload localy
        // var files = $('input[type=file]').get(0).files.item(0);
        // var fileName = $('input[type=file]').val().replace(/.*(\/|\\)/, '');
        // alert(fileName);
        // alert(ID);
        // var fd = new FormData();
        //
        //     fd.append("photo", files);
        //     var link = undefined;
        //      $.ajax({
        //         type: 'POST',
        //         url: ID+"/"+fileName+'/upload',
        //         data: fd,
        //         processData: false,
        //         contentType: false,
        //          async: false
        //     }).done(function(data) {
        //         var obj = JSON.parse(data);
        //         alert(obj.src);
        //         link = obj.src;
        //         return false;
        //     });
        //      return link;

        var files =  $('input[type=file]').get(0).files.item(0);
        var fileName = $('input[type=file]').val().replace(/.*(\/|\\)/, '');



        var apiUrl = 'https://api.imgur.com/3/image';
        var apiKey = '43d001a53634a90';

        var formData = new FormData();
        formData.append("image", files);

        var link = "";

        $.ajax({
            type: 'POST',
            url: apiUrl,
            data: formData,
            processData: false,
            contentType: false,
            async: false,
            headers: {
                Authorization: 'Client-ID ' + apiKey,
                Accept: 'application/json'
            },
            mimeType: 'multipart/form-data'
        }).done(function(response) {
            var obj = JSON.parse(response);
            link = obj.data.link;
        });

        return link;
    },

    uploadToDB : function (link) {
        var files =  $('input[type=file]').get(0).files.item(0);
        var fileName = $('input[type=file]').val().replace(/.*(\/|\\)/, '');
        $.ajax({
            type: 'POST',
            url: User.visitedUserId+"/"+fileName+'/upload',
            processData: false,
            contentType: false,
            data : JSON.stringify({
                "src": link ,
                "imageType":Image.currentCategory ,
                "id" : Utilities.idGenerator("PH") ,
                "ownerId" : Utilities.cookieValue("id") ,
                "entityId" : Table.tableStack[Table.counter - 1].editModeId
            }),
            async: false
        }).done(function(data) {
        });
    }

};

function testImage(){
    var link = Image.upload(User.visitedUserId);

    if(link !== ""){
        Table.currentTable.link = link;
    }
    else {
        alert("error in upload to imgur");
    }
}