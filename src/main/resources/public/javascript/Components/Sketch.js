var Sketch = {

    currentCategory : "rigging" ,

    handleCategories : function(category,btn){
        $(".sketchButtons").removeClass("active");
        $(btn).addClass("active");
        Sketch.currentCategory = btn.getAttribute("id").replace("Button","");
        var i;
        var categories = document.getElementsByClassName("Scategories");
        for(i = 0; i < 3; i++){
            categories[i].style.display = "none";
        }
        document.getElementById(category).style.display = "block";
    },


    renderButtons : function(){

        return '<div id="sketchButtons" style="margin-bottom: 11px;">' +
            ' <h3>Sketches</h3>' +
            ' <button type="button" id="riggingButton" class="btn btn-primary sketchButtons active" onclick="Sketch.handleCategories(\'riggingCategory\',this)">Rigging</button>' +
            ' <button type="button" id="roomButton" class="btn btn-primary sketchButtons" onclick="Sketch.handleCategories(\'roomCategory\',this)">Room</button>' +
            ' <button type="button" id="caveButton" class="btn btn-primary sketchButtons" onclick="Sketch.handleCategories(\'caveCategory\',this)">Cave</button>' +
            '<div class="upload-btn-wrapper pull-right">'+
            '<button id="SuploadBtn">Upload a file</button>'+
            '<input type="file" class="imgur" name="myfile" accept="image/*" onchange="testSketch();"/>'+
            '</div>'+
            '</div>';
    },

    render : function (modalMode,id,targetId) {
        var toAdd;
        var tableId;

        if(id === undefined)
            tableId = Table.currentTable.id;
        else{
            tableId = id;
        }

        if(modalMode === undefined) {

            toAdd = document.getElementById("sketchTabContent");
            toAdd.innerHTML = '' +
                Sketch.renderButtons()+
                ' <div class="row">' +
                ' <div class="container">' +
                ' <div id="riggingCategory" class="Scategories">' +
                ' </div>' +
                ' <div id="roomCategory" class="Scategories" style="display: none">' +
                ' </div>' +
                ' <div id="caveCategory" class="Scategories" style="display: none">' +
                ' </div>' +
                ' </div>' +
                '</div>';

            if(Table.currentTable.id === "sketches"){
                document.getElementById("SuploadBtn").style.display = 'none';
            }

            $.ajax({
                type: 'GET',
                url: "/api/Caves/"+User.visitedUserId+"/sketches",
                processData: false,
                contentType: false,
                async: false
            }).done(function(data) {
                var obj = JSON.parse(data);
                for(var i = 0; i < obj.length; i++)
                {
                    if(obj[i].sketchType !== null) {
                        document.getElementById(obj[i].sketchType + "Category").innerHTML += ' <div class="col-lg-3 col-sm-4 col-6">' +
                            '<div class="thumbnail">' +
                            '<img ' +
                            'src="'+obj[i].link+'" class="thumbnail img-responsive wh">'+
                            '<div class="caption">'+
                            '<p>Uploaded by : '+obj[i].username+'<br>Date: '+obj[i].date+'</p>'+
                            '</div>'+
                            '</div>';
                    }
                }
            });
        }
        else{

            var c = Table.counter - 1 ;
            if(Table.counter ===  1){
                document.getElementById("toAddIndi").innerHTML += '<li data-target="#' + targetId + 'Modal" data-slide-to="'+c+'" class="active"></li>';
                toAdd = document.getElementById("toAddItem");
                toAdd.innerHTML += '' +
                    '<div class="item active">' +
                    Sketch.renderButtons() +
                    ' <div class="row">' +
                    ' <div class="container">' +
                    ' <div id="riggingCategory" class="Scategories">' +
                    ' </div>' +
                    ' <div id="roomCategory" class="Scategories" style="display: none">' +
                    ' </div>' +
                    ' <div id="caveCategory" class="Scategories" style="display: none">' +
                    ' </div>' +
                    ' </div>' +
                    '</div>'+
                    '</div>';


                $.ajax({
                    type: 'GET',
                    url: "/api/Caves/"+User.visitedUserId+"/"+Table.tableStack[Table.counter - 1].id+"/"+Table.rowStack[Table.counter - 1]+"/"+Table.currentTable.id,
                    processData: false,
                    contentType: false,
                    async: false
                }).done(function(data) {
                    var obj = JSON.parse(data);
                    for(var i = 0; i < obj.length; i++)
                    {
                        alert(obj[i].sketchType);

                        if(obj[i].sketchType !== null) {
                            document.getElementById(obj[i].sketchType + "Category").innerHTML += ' <div class="col-lg-3 col-sm-4 col-6">' +
                                '<div class="thumbnail">' +
                                '<img ' +
                                'src="'+obj[i].link+'" class="thumbnail img-responsive wh">'+
                                '<div class="caption">'+
                                '<p>Uploaded by : '+obj[i].username+'<br>Date: '+obj[i].date+'</p>'+
                                '</div>'+
                                '</div>';
                        }
                    }
                });

            }
            else if(Table.counter >=  1){

                document.getElementById("toAddIndi").innerHTML += '<li id="trigger" data-target="#' + targetId + 'Modal" data-slide-to="'+c+'"></li>';
                toAdd = document.getElementById("toAddItem");
                toAdd.innerHTML += '' +
                    '<div class="item">' +
                    Sketch.renderButtons() +
                    ' <div class="row">' +
                    ' <div class="container">' +
                    ' <div id="riggingCategory" class="Scategories">' +
                    ' </div>' +
                    ' <div id="roomCategory" class="Scategories" style="display: none">' +
                    ' </div>' +
                    ' <div id="caveCategory" class="Scategories" style="display: none">' +
                    ' </div>' +
                    ' </div>' +
                    '</div>'+
                    '</div>';

                document.getElementById("SuploadBtn").style.display = 'none';

                $.ajax({
                    type: 'GET',
                    url: "/api/Caves/"+User.visitedUserId+"/"+Table.tableStack[Table.counter - 1].id+"/"+Table.rowStack[Table.counter - 1]+"/"+Table.currentTable.id,
                    processData: false,
                    contentType: false,
                    async: false
                }).done(function(data) {
                    var obj = JSON.parse(data);
                    for(var i = 0; i < obj.length; i++)
                    {
                        alert(obj[i].sketchType);

                        if(obj[i].sketchType !== null) {
                            document.getElementById(obj[i].sketchType + "Category").innerHTML += ' <div class="col-lg-3 col-sm-4 col-6">' +
                                '<div class="thumbnail">' +
                                '<img ' +
                                'src="'+obj[i].link+'" class="thumbnail img-responsive wh">'+
                                '<div class="caption">'+
                                '<p>Uploaded by : '+obj[i].username+'<br>Date: '+obj[i].date+'</p>'+
                                '</div>'+
                                '</div>';
                        }
                    }
                });

                $('#trigger').click();
                document.getElementById("trigger").removeAttribute("id");

            }

        }

    } ,

    upload : function (ID) {
        var files =  $('input[type=file]').get(0).files.item(0);
        var fileName = $('input[type=file]').val().replace(/.*(\/|\\)/, '');



        var apiUrl = 'https://api.imgur.com/3/image';
        var apiKey = '43d001a53634a90';

        var formData = new FormData();
        formData.append("image", files);

        var link = "";

        $.ajax({
            type: 'POST',
            url: apiUrl,
            data: formData,
            processData: false,
            contentType: false,
            async: false,
            headers: {
                Authorization: 'Client-ID ' + apiKey,
                Accept: 'application/json'
            },
            mimeType: 'multipart/form-data'
        }).done(function(response) {
            var obj = JSON.parse(response);
            link = obj.data.link;
        });

        return link;
    }

};

function testSketch(){
    var link = Sketch.upload(User.visitedUserId);

    if(link !== ""){
        Table.currentTable.link = link;
    }
    else {
        alert("error in upload to imgur");
    }
}