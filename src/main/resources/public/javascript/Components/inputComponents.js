var autocompleteSrc = [];
var usernameToId = {};
var duplicates = [];

function inputTextRender(inputToAdd,i) {
    var inputGroup = document.createElement('DIV');
    inputGroup.setAttribute('class','input-group');

    var input = document.createElement("INPUT");
    input.setAttribute("type","text");
    input.setAttribute("placeholder",inputToAdd.label);
    input.setAttribute("id",inputToAdd.id);
    input.setAttribute("class","form-control");

    var errorSpan = document.createElement('SPAN');
        errorSpan.setAttribute("id",inputToAdd.id+"Error");
        errorSpan.innerHTML =
            '<label style="color: red;">'+inputToAdd.errorText+'</label>';
        errorSpan.style.display = 'none';

    inputGroup.appendChild(input);
    inputGroup.appendChild(errorSpan);
    document.getElementById("inputsField"+i).appendChild(inputGroup);

}

function textAreaRender(inputToAdd,i) {
    var formGroup = document.createElement('DIV');
    formGroup.setAttribute('class','form-group');

    var textArea = document.createElement('textarea');
    textArea.setAttribute('id',inputToAdd.id);
    textArea.setAttribute('rows','5');
    textArea.setAttribute('class','col-md-5');

    formGroup.appendChild(textArea);
    document.getElementById("inputsField"+i).appendChild(formGroup);
}

function inputRadioRender(inputToAdd,i,radios) {
    for(var k in radios) {
        document.getElementById("inputsField"+i).innerHTML += ' ' +
            '<label>' +
            '<input id="'+inputToAdd.id+'" name="'+inputToAdd.id+'" type="radio" value="'+radios[k]+'">' +
            ' '+radios[k]+' </label>';
    }
}

function uploadButtonRender(i) {
document.getElementById("inputsField" + i).innerHTML += '' +
    '<div class="input-group"> <span class="input-group-addon" id="file_upload"><i class="glyphicon glyphicon-upload"></i></span>' +
        '<input type="file" accept="image/*" data-max-size="5000" id="file_nm" class="form-control upload imgur" aria-describedby="file_upload">' +
    '</div>';
}

function getValue() {
    return document.getElementById(this.id).value;
}

function getAdmin() {
    var values = document.getElementsByClassName("adminInputs");
    var id = [];
    for(var i = 0; i < values.length-1; i++){
            id.push(usernameToId[values[i].value]);
    }
    return id;
}

function getParticipatingClub() {
    return usernameToId[document.getElementById(this.id).value];
}



function getDate() {
    var date = $("#foundedDate").datepicker("getDate");
    return (date.getMonth() + 1)  + "/" + date.getDate() + "/" + date.getFullYear();
}

function inputTextRenderDatePicker(inputToAdd,i) {

    document.getElementById("inputsField"+i).innerHTML = '' +
        '<div class="input-group date col-md-5" data-provide="datepicker">'   +
        ' <input type="text" id="'+this.id+'" class="form-control" placeholder="mm/dd/yyyy">'+
        ' <div class="input-group-addon">'+
        ' <span class="glyphicon glyphicon-th"></span>'+
        ' </div>'+
        '</div>';

}

function inputMultipleRender(inputToAdd,i) {


    document.getElementById("inputsField" + i).innerHTML += '' +
        '<div class="col-md-5 input-group">' +
         '<input type="text" class="form-control adminInputs">'+
       ' <span class="input-group-btn">'+
       ' <button type="button" class="btn btn-md  btn-success btn-number remove-btn" style="visibility: hidden">'+
       ' <i class="glyphicon glyphicon-minus"></i>'+
       ' </button>'+
       '</span>' +
       '</div>'+
        '<br>';
    document.getElementById("inputsField" + i).innerHTML += '<br><div id="addRow">' +
        '<button id="btnAddRow" class="btn btn-success btn-md" onclick="addRow();">Add Row</button>' +
        '</div>';

}
function addRow() {

    var newRow = $( '<div class="col-md-5 input-group">' +
        '<input type="text" class="form-control adminInputs">'+
        ' <span class="input-group-btn">'+
        ' <button type="button" class="btn btn-md  btn-success btn-number remove-btn" style="visibility: hidden">'+
        ' <i class="glyphicon glyphicon-minus"></i>'+
        ' </button>'+
        '</span>' +
        '</div>'+
        '<br>');

    var id = document.getElementsByClassName("adminInputs");

    if (id.length >= 0) {
        if(autocompleteSrc.includes(id[id.length-1].value))
        {
            if(!duplicates.includes(id[id.length-1].value)){
                $('.adminInputs').prop('readonly',true);
                $('.btn').css('visibility', 'visible');
                duplicates.push(id[id.length-1].value);
                newRow.insertBefore('#addRow');
                User.cavingClub.autocomplete();
            }
            else{
                alert("duplicate admin");
            }

                return true;

        }
        else {
            alert("not valid admin");
        }
    }
}

$(document).on('click', '.remove-btn', function(){
    var index = duplicates.indexOf($(this).closest('.input-group').find('input').val());
    duplicates.splice(index, 1);
    $(this).closest('.input-group').remove();
});