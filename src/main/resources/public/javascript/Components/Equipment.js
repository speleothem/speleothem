var Equipment = {

  render : function (modalMode,id,targetId) {
      var toAdd;
      if(modalMode === undefined){
          document.getElementById("equipmentTabContent").innerHTML = '' +
              '<br>' +
              '<br>' +
              '<div id="addEqBtn"></div>' +
              '<div id="MainMenu" style="width: 50%; margin-left: 24%;">'+
              '<div class="list-group panel" id="toAppendItems">'+
              '<a class="list-group-item list-group-item active strong" data-parent="#MainMenu">Equipment<p class="pull-right">Quantity</p></a>'+
              '</div>'+
              '</div>';
          Equipment.renderEqBtn();
          Equipment.renderItem("Backpacks","Backpacks");
          Equipment.renderItem("Cavepacks","Cavepacks");
          Equipment.renderItem("ElectricityGenerator","Electricity Generator");
          Equipment.renderItem("Lighting","Lighting");
          Equipment.renderItem("MeasuringInstruments","Measuring Instruments");
          Equipment.renderItem("NavigationInstrument","Navigation Instruments");
          Equipment.renderItem("RecordingTools","Recording Tools");
          Equipment.renderItem("ProtectiveClothing","Protective Clothing");
          Equipment.renderSubMenu("ProtectiveClothing","Aluminum Blanket");
          Equipment.renderSubMenu("ProtectiveClothing","Helmets");

          Utilities.ajaxCallGET('/api/UserProfiles/'+User.currentUser.name+'/'+Utilities.cookieValue("id")+'/Equipment').done(function(data){
              var obj = JSON.parse(data);
              var description = "";
              for(var i in obj){
                  description = obj[i].description;
                  description = description.split('[newline]').join("\n");
                  Equipment.renderSubItem(obj[i].type,description,obj[i].quantity);
              }
          });
      }
      else {
          var c = Table.counter - 1 ;
          document.getElementById("toAddIndi").innerHTML += '<li data-target="#' + targetId + 'Modal" data-slide-to="'+c+'" class="active"></li>';
          toAdd = document.getElementById("toAddItem");
          toAdd.innerHTML += '<br>' +
              '<br>' +
              '<div id="addEqBtn"></div>' +
              '<div id="MainMenu" style="width: 50%; margin-left: 24%;">' ;

          if(Table.tableStack[Table.counter - 1].editModeId !== undefined){
              toAdd.innerHTML += Modal.equipmentAdd.renderContent();
              document.getElementById("submitEq").style.display = "none";
          }
          else {
              toAdd.innerHTML +=  '<div class="list-group panel" id="toAppendItems">'+
                  '<a class="list-group-item list-group-item active strong" data-parent="#MainMenu">Equipment<p class="pull-right">Quantity</p></a>'+
                  '</div>'+
                  '</div>';
              // Equipment.renderEqBtn();
              Equipment.renderItem("Backpacks","Backpacks");
              Equipment.renderItem("Cavepacks","Cavepacks");
              Equipment.renderItem("ElectricityGenerator","Electricity Generator");
              Equipment.renderItem("Lighting","Lighting");
              Equipment.renderItem("MeasuringInstruments","Measuring Instruments");
              Equipment.renderItem("NavigationInstrument","Navigation Instruments");
              Equipment.renderItem("RecordingTools","Recording Tools");
              Equipment.renderItem("ProtectiveClothing","Protective Clothing");
              Equipment.renderSubMenu("ProtectiveClothing","Aluminum Blanket");
              Equipment.renderSubMenu("ProtectiveClothing","Helmets");


              Utilities.ajaxCallGET("/api/Caves/"+User.visitedUserId+"/"+Table.tableStack[Table.counter - 1].id+"/"+Table.rowStack[Table.counter - 1]+"/"+Table.currentTable.id).done(function(data){
                  var obj = JSON.parse(data);
                  var description = "";
                  for(var i in obj){
                      description = obj[i].description;
                      description = description.split('[newline]').join("\n");
                      Equipment.renderSubItem(obj[i].type,description,obj[i].quantity);
                  }
              });
          }

      }

      if(User.visitedUserId !== Utilities.cookieValue("id")){
          document.getElementById("riggingEqBtn").style.display = 'none';
      }
  } ,

    renderRiggingEquipment : function (modalMode,id,targetId) {
      var toAdd;
        if(modalMode === undefined){
            document.getElementById("riggingEquipmentTabContent").innerHTML = '' +
                '<br>' +
                '<br>' +
                '<div id="addEqBtnRigging"></div>' +
                '<div id="MainMenu" style="width: 50%; margin-left: 24%;">'+
                '<div class="list-group panel" id="toAppendItemsRigging">'+
                '<a class="list-group-item list-group-item active strong" data-parent="#MainMenu">Rigging Equipment<p class="pull-right">Quantity</p></a>'+
                '</div>'+
                '</div>';
            Equipment.renderEqBtn();
            Equipment.renderItemRigging("DrillBatteries","Drill Batteries");
            Equipment.renderItemRigging("Drills","Drills");
            Equipment.renderItemRigging("Dyneemas","Dyneemas");
            Equipment.renderItemRigging("Plaques","Plaques");
            Equipment.renderItemRigging("RiggingSets","Rigging Sets");
            Equipment.renderItemRigging("Tapes","Tapes");
            Equipment.renderItemRigging("Carabiners","Carabiners");
            Equipment.renderSubMenu("Carabiners","Locking Carabiners");
            Equipment.renderSubMenu("Carabiners","Non LockingCarabiners");
            Utilities.ajaxCallGET('/api/UserProfiles/'+User.currentUser.name+'/'+Utilities.cookieValue("id")+'/RiggingEquipment').done(function(data){
                var obj = JSON.parse(data);
                var description = "";
                for(var i in obj){
                    description = obj[i].description;
                    description = description.split('[newline]').join("\n");
                    Equipment.renderSubItem(obj[i].type,description,obj[i].quantity);
                }
            });
        }
        else{
            var c = Table.counter - 1 ;
            document.getElementById("toAddIndi").innerHTML += '<li data-target="#' + targetId + 'Modal" data-slide-to="'+c+'" class="active"></li>';
            toAdd = document.getElementById("toAddItem");
            toAdd.innerHTML += '<br>' +
                '<br>' +
                '<div id="addEqBtnRigging"></div>' +
                '<div id="MainMenu" style="width: 50%; margin-left: 24%;">';

            if(Table.tableStack[Table.counter - 1].editModeId !== undefined){
                toAdd.innerHTML += Modal.rigging_equipmentAdd.renderContent();
                document.getElementById("submitEq").style.display = "none";
            }
            else {
                toAdd.innerHTML +=  '<div class="list-group panel" id="toAppendItemsRigging">'+
                    '<a class="list-group-item list-group-item active strong" data-parent="#MainMenu">Rigging Equipment<p class="pull-right">Quantity</p></a>'+
                    '</div>'+
                    '</div>';
                // Equipment.renderEqBtn();
                Equipment.renderItemRigging("DrillBatteries","Drill Batteries");
                Equipment.renderItemRigging("Drills","Drills");
                Equipment.renderItemRigging("Dyneemas","Dyneemas");
                Equipment.renderItemRigging("Plaques","Plaques");
                Equipment.renderItemRigging("RiggingSets","Rigging Sets");
                Equipment.renderItemRigging("Tapes","Tapes");
                Equipment.renderItemRigging("Carabiners","Carabiners");
                Equipment.renderSubMenu("Carabiners","Locking Carabiners");
                Equipment.renderSubMenu("Carabiners","Non LockingCarabiners");
                Utilities.ajaxCallGET("/api/Caves/"+User.visitedUserId+"/"+Table.tableStack[Table.counter - 1].id+"/"+Table.rowStack[Table.counter - 1]+"/"+Table.currentTable.id).done(function(data){
                    var obj = JSON.parse(data);
                    alert(data);
                    var description = "";
                    for(var i in obj){
                        description = obj[i].description;
                        description = description.split('[newline]').join("\n");
                        Equipment.renderSubItem(obj[i].type,description,obj[i].quantity);
                    }
                });
            }
        }
        if(User.visitedUserId !== Utilities.cookieValue("id")){
            document.getElementById("eqBtn").style.display = 'none';
        }
    } ,


    renderSubItem : function (parentID,description,quantity) {
        document.getElementById(parentID).innerHTML += '<a href="#" data-parent="#'+parentID+'" class="list-group-item">'+description+'<p class="pull-right">'+quantity+'</p></a>';
    } ,
    renderSubMenu : function (parentID,label) {
       document.getElementById(parentID).innerHTML += '<a href="#'+label.replace(" ","")+'" class="list-group-item strong" data-toggle="collapse" data-parent="#'+parentID+'">'+label+'<i class="fa fa-caret-down"></i></a>' +
              '<div class="collapse list-group-submenu" id="'+label.replace(" ","")+'">' +
              '</div>';

    } ,
    renderItem : function (id,label) {
        document.getElementById("toAppendItems").innerHTML += '<a href="#'+id+'" class="list-group-item list-group-item-success strong" data-toggle="collapse" data-parent="#MainMenu">'+label+'<i class="fa fa-caret-down"></i></a>'+
               '<div class="collapse" id="'+id+'">' +
               '</div>';
    } ,
    renderItemRigging : function (id,label) {
        document.getElementById("toAppendItemsRigging").innerHTML += '<a href="#'+id+'" class="list-group-item list-group-item-success strong" data-toggle="collapse" data-parent="#MainMenu">'+label+'<i class="fa fa-caret-down"></i></a>'+
            '<div class="collapse" id="'+id+'">' +
            '</div>';
    } ,
    renderEqBtn : function () {
      var toAdd;
      var button;
      if(User.currentTab === "RIGGINGEQ"){
          toAdd = "addEqBtnRigging";
          button = "eqBtn";
      }
      else {
          toAdd = "addEqBtn";
          button = "riggingEqBtn";
      }

        document.getElementById(toAdd).innerHTML = '' +
            '<div class="btn-group pull-right">'+
                '   <button id='+button+' data-target = "#emptyModal" data-toggle = "modal" type="button" class="btn btn-success dropdown-toggle">Add Equipment</button>'+
            '</div>' +
            '<br>'+
            '<br>'+
            '<br>';

        document.getElementById(button).addEventListener("click",function () {
            Modal.create();
            if(User.currentTab === "EQUIPMENT"){
                Modal.equipmentAdd.render();
            }
            else if(User.currentTab === "RIGGINGEQ"){
                Modal.rigging_equipmentAdd.render();
            }
        },false);

    } ,
    sendEquipment : function () {
      var url;
      if(User.currentTab === "EQUIPMENT" || User.currentTab === "RIGGINGEQ"){
          url = '/api/UserProfiles/'+User.currentUser.name+"/"+Utilities.cookieValue("id")+"/add" + Table.currentTable.id;
      }
        $.ajax({
            method: 'POST',
            url: url,
            data: JSON.stringify({
                "description"  : document.getElementById("description").value,
                "quantity"  : document.getElementById("quantity").value,
                "type"  : document.getElementById("chooseEquipment").value,
                "id" : Utilities.idGenerator("EQ")
            }),
            success: function (data) {
                alert("Equipment added successfully");
            }
        });
    }

};