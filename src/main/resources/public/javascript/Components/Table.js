var Table = {

    currentTable : undefined,
    tableStack : [] ,
    tableStackId : [] ,
    rowStack : [],
    counter : 0 ,
    change : function (changeState,modalMode,id) {
        if(!modalMode){
            Table.currentTable = changeState;
            Table.tableStack[Table.counter] = Table.currentTable;
            Table.currentTable.render();
        }
        else{
            Table.counter++;
            Table.currentTable = changeState;
            Table.tableStack[Table.counter] = Table.currentTable;
            Table.tableStackId[Table.counter] = id;
            if(Table.currentTable === Table.PHOTOS)
                Modal.change(Modal.imageModal,id);
            else if(Table.currentTable === Table.SKETCHES)
                Modal.change(Modal.sketchModal,id);
            else if(Table.currentTable === Table.EQUIPMENT)
                Modal.change(Modal.equipmentModal,id);
            else if(Table.currentTable === Table.RIGGINGEQ)
                Modal.change(Modal.riggingEquipmentModal,id);
            else
                Modal.change(Modal.tb,id);

        }

    } ,

    tableColumns : {
        action : {label : "Action",render : actionButtons,type : "actionButton",autocomplete : undefined} ,
        check : {label : "Choose",render : checkBoxes,type : "checkbox",autocomplete : undefined} ,
        type : {label : "Type", render : textInput,type : "Text",autocomplete : autocompleteText} ,
        club : {label : "Club",render : textInput,type : "Text",autocomplete : autocompleteText} ,
        startDate : {label : "Start Date",render : textInput,type : "Text",autocomplete : undefined} ,
        finishedDate : {label : "Finished Date",render : textInput,type : "Text",autocomplete : undefined} ,
        estimatedDate : {label : "Estimated Date",render : textInput,type : "Text",autocomplete : undefined} ,
        foundDate : {label : "Found Date",render : textInput,type : "Text",autocomplete : undefined} ,
        date : {label : "Date",render : textInput,type : "Text",autocomplete : undefined} ,
        informer : {label : "Informer",render : textInput,type : "Text",autocomplete : undefined} ,
        leader : {label : "Leader",render : textInput,type : "Text",autocomplete : undefined} ,
        riggings : {label : "Rigging",render : buttonModal,type : "Button",autocomplete : undefined} ,
        cavers : {label : "Cavers",render : buttonModal,type : "Button",autocomplete : undefined} ,
        description : {label : "Description",render : descriptionTextarea,type : "description",autocomplete : undefined} ,
        equipment : {label : "Equipment",render : buttonModal,type : "Button",autocomplete : undefined} ,
        riggingEquipment : {label : "Rigging equipment",render : buttonModal,type : "Button",autocomplete : undefined} ,
        sketch : {label : "Sketch",render : buttonModal,type : "Button",autocomplete : undefined} ,
        firstName : {label : "First Name",render : textInput,type : "Text",autocomplete : undefined} ,
        lastName : {label : "Last Name",render : textInput,type : "Text",autocomplete : undefined} ,
        username : {label : "Username",render : textInput,type : "Text",autocomplete : autocompleteText} ,
        isMain : {label : "Main",render : textInput,type : "Text",autocomplete : undefined},
        orientation : {label : "Orientation",render : textInput,type : "Text",autocomplete : undefined},
        size : {label : "Size",render : textInput,type : "Text",autocomplete : undefined},
        latitude : {label : "Latitude",render : textInput,type : "Text",autocomplete : undefined},
        longitude : {label : "Longitude",render : textInput,type : "Text",autocomplete : undefined},
        photo : {label : "Photo",render : buttonModal,type : "Button",autocomplete : undefined},
        speleothems : {label : "Speleothems",render : buttonModal,type : "Button",autocomplete : undefined},
        organisms : {label : "Organisms",render : buttonModal,type : "Button",autocomplete : undefined},
        usage : {label : "Usage",render : textInput,type : "Text",autocomplete : autocompleteText},
        temperature : {label : "Temperature",render : textInput,type : "Text",autocomplete : undefined},
        airCurrents : {label : "Air currents",render : textInput,type : "Text",autocomplete : undefined},
        humidity : {label : "Humidity",render : textInput,type : "Text",autocomplete : undefined},
        name : {label : "Name",render : textInput,type : "Text",autocomplete : undefined},
        // class : {label : "Class",render : textInput,type : "Text",autocomplete : autocompleteText},
        depth : {label : "Depth",render : textInput,type : "Text",autocomplete : undefined},
        room : {label : "Room",render : buttonModal,type : "Button",autocomplete : undefined},
        expeditions : {label : "Expeditions",render : buttonModal,type : "Button",autocomplete : undefined}
    } ,

    createColumns : function (columns,id) {
        var tableId = id;

        for(var i in columns) {
            if(this.tableColumns[columns[i]].label === "Choose"){
                if(Table.counter >=  1) {
                    document.getElementById(tableId + "Columns").innerHTML += '' +
                        '<th class="col-md-1">' + this.tableColumns[columns[i]].label + '</th>';
                }
            }
            else if(this.tableColumns[columns[i]].label === "Action"){
                    document.getElementById(tableId + "Columns").innerHTML += '' +
                        '<th class="col-md-1 Actions">' + this.tableColumns[columns[i]].label + '</th>';
            }
            else{
                document.getElementById(tableId + "Columns").innerHTML += '' +
                    '<th class="col-md-1">' + this.tableColumns[columns[i]].label + '</th>';
            }


        }

        document.getElementById(tableId+"Columns").innerHTML += '' +
            '<th class="col-md-1" xmlns="http://www.w3.org/1999/html"><button type="button" class="btn btn-default btn-sm" id="'+tableId+'Add_row">' +
            '<span class="glyphicon glyphicon-plus"></span></span></th>';

        if(Table.counter > 1){
            document.getElementById(tableId+"Add_row").style.display = 'none';
        }
        if(Table.counter === 0){
            if(User.visitedUser === User.cavingClub && User.currentTab === "MEMBERS"){
                Utilities.ajaxCallGET("/api/UserProfiles/CavingClubs/"+User.visitedUserId+"/"+this.currentTable.id).done(function(data){
                    var obj = JSON.parse(data);
                    for(var i in obj){
                        Table.createRowWithValues(columns,id,obj[i]);
                    }
                });
            }
            else {
                Utilities.ajaxCallGET("/api/Caves/"+User.visitedUserId+"/"+this.currentTable.id).done(function(data){
                    var obj = JSON.parse(data);
                    for(var i in obj){
                        Table.createRowWithValues(columns,id,obj[i]);
                    }
                });
            }
        }
        else if(Table.counter >= 1){
            if(Table.tableStack[Table.counter - 1].editModeId === undefined){
                Utilities.ajaxCallGET("/api/Caves/"+User.visitedUserId+"/"+Table.tableStack[Table.counter - 1].id+"/"+Table.rowStack[Table.counter - 1]+"/"+Table.currentTable.id).done(function(data){
                    var obj = JSON.parse(data);
                    for(var i in obj){
                        Table.createRowWithValues(columns,id,obj[i]);
                    }
                });
                document.getElementById(tableId+"Add_row").style.display = 'none';
                readModeButtons(true);
            }
            else {
                Utilities.ajaxCallGET("/api/Caves/"+User.visitedUserId+"/"+Table.currentTable.id).done(function(data){
                    var obj = JSON.parse(data);
                    for(var i in obj){
                        Table.createRowWithValues(columns,id,obj[i]);
                    }
                });
            }
        }

        if(User.currentTab === "CAVERS" || User.currentTab === "MEMBERS"){
            readModeButtons(true);
            document.getElementById(tableId+"Add_row").style.display = 'none';
        }

        if(User.currentTab === "EXPEDITIONS" && User.visitedUserId.substring(0, 2) !== "CV"){
            readModeButtons(true);
            document.getElementById(tableId+"Add_row").style.display = 'none';
        }

        document.getElementById(tableId+"Add_row").onclick = function () {
            if(Table.currentTable.editModeId === undefined)
                Table.createRow(columns,id);
        }

    } ,

    createRowWithValues : function (columns,id2,values) {
        var allPropertyNames = Object.keys(values);
        var x = document.createElement("TR");
        var elem;
        x.setAttribute("id",values.id);
        var k = 0;
        for (var i in columns){
            elem = Table.tableColumns[columns[i]];
            if(this.tableColumns[columns[i]].label === "Choose"){
                if(Table.counter >=  1) {
                    x.innerHTML += '' +
                        '<td>' +
                        elem.render(values.id) +
                        '</td>';
                }
            }
            else if(this.tableColumns[columns[i]].type === "Text"){
                x.innerHTML += '' +
                    '<td>' +
                    elem.render(values[allPropertyNames[k]],values["externalSource"]) +
                    '</td>';

            }
            else if(this.tableColumns[columns[i]].type === "description"){
                x.innerHTML += '' +
                    '<td>' +
                    elem.render(values["description"]) +
                    '</td>';

            }
            else {
                x.innerHTML += '' +
                    '<td>' +
                    elem.render(values.id) +
                    '</td>';
            }
            k++;
        }

        x.innerHTML += '<td></td>';
        document.getElementById(id2+'TableBody').appendChild(x);

        var buttons = document.getElementsByClassName("buttonModals");
        for (var j = 0; j < buttons.length; j++) {
            buttons[j].addEventListener('click', changeModalTable , false);
            buttons[j].id2 = buttons[j].getAttribute("href");
            buttons[j].id2 = buttons[j].id2.replace("#","");
            buttons[j].id2 = buttons[j].id2.replace("Modal","");
            buttons[j].name = buttons[j].getAttribute("name");
        }

        $("#"+values.id).find("input[type=text]").each(function() {
            $(this).prop('readonly', true);
            Table.currentTable.item [$(this).attr("placeholder")] = $(this).val();
        });
        $("#"+values.id).find("textarea").each(function() {
            $(this).prop('readonly', true);
            Table.currentTable.item [$(this).attr("placeholder")] = $(this).val();
        });

        $("#"+values.id).find("#conf_row"+values.id).css('display','none');
        $("#"+values.id).find("#edit_row"+values.id).css('display','block');

    },

    createRow : function (columns,id2) {

        // this.tableStack[this.counter] = this.currentTable;

        var tableId;
        if(id2 === undefined)
            tableId = Table.currentTable.id;
        else{
            tableId = id2;
        }

        var id = Utilities.idGenerator(Table.currentTable.prefix);
        var x = document.createElement("TR");
        var elem;

        x.setAttribute("id",id);

        for (var i in columns){
            elem = Table.tableColumns[columns[i]];
            if(this.tableColumns[columns[i]].label === "Choose"){
                if(Table.counter >=  1) {
                    x.innerHTML += '' +
                        '<td>' +
                        elem.render(id) +
                        '</td>';
                }
            }
            else if(this.tableColumns[columns[i]].type === "Text" || this.tableColumns[columns[i]].type === "description"){
                x.innerHTML += '' +
                    '<td>' +
                    elem.render(undefined) +
                    '</td>';
            }
            else{
                x.innerHTML += '' +
                    '<td>' +
                    elem.render(id) +
                    '</td>';
            }

        }

        x.innerHTML += '<td></td>';
        Table.currentTable.editModeId = id;
        document.getElementById(tableId + 'TableBody').appendChild(x);

        for (var z in columns){
            elem = Table.tableColumns[columns[z]];
            if(this.tableColumns[columns[z]].type === "Text"){
                if(elem.autocomplete !== undefined){
                    elem.autocomplete();
                }
            }
        }

        var buttons = document.getElementsByClassName("buttonModals");
        for (var j = 0; j < buttons.length; j++) {
            buttons[j].addEventListener('click', changeModalTable, false);
            buttons[j].id2 = buttons[j].getAttribute("href");
            buttons[j].id2 = buttons[j].id2.replace("#", "");
            buttons[j].id2 = buttons[j].id2.replace("Modal", "");
            buttons[j].name = buttons[j].getAttribute("name");
        }

        $('[data-toggle="popover"]').popover();

    } ,


    renderTable : function (modalMode,id,targetId) {
        var toAdd;
        var tableId;

        if(id === undefined)
            tableId = Table.currentTable.id;
        else{
            tableId = id;
        }

        if(modalMode === undefined){
            toAdd = document.getElementById(tableId+"TabContent");
            toAdd.innerHTML = '' +
                '<div id="tableContent" class="container-fluid">' +
                '<div class="row">'+
                ' <div class="table-responsive">'+
                '<table class="table table-hover" id="'+tableId+'Table">'+
                '<thead>'+
                '<tr id="'+tableId+'Columns">'+
                // add table columns here
                ' </tr>'+
                '</thead>'+
                '<tbody id="'+tableId+'TableBody">' +

                '</tbody>'+
                '</table>'+
                '</div>'+
                ' </div>'+
                '</div>';
        }
        else{
            var c = Table.counter - 1 ;
            if(Table.counter ===  1){
                document.getElementById("toAddIndi").innerHTML += '<li data-target="#' + targetId + 'Modal" data-slide-to="'+c+'" class="active"></li>';
                toAdd = document.getElementById("toAddItem");
                toAdd.innerHTML += '' +
                    '<div class="item active">' +
                    '<div id="tableContent" class="container-fluid">' +
                    '<div class="row">'+
                    ' <div class="table-responsive">'+
                    '<table class="table table-hover" id="'+tableId+'Table">'+
                    '<thead>'+
                    '<tr id="'+tableId+'Columns">'+
                    // add table columns here
                    ' </tr>'+
                    '</thead>'+
                    '<tbody id="'+tableId+'TableBody">' +

                    '</tbody>'+
                    '</table>'+
                    '</div>'+
                    ' </div>'+
                    ' </div>'+
                    '</div>';
            }
            else if(Table.counter >=  1){

                document.getElementById("toAddIndi").innerHTML += '<li id="trigger" data-target="#' + targetId + 'Modal" data-slide-to="'+c+'"></li>';
                toAdd = document.getElementById("toAddItem");
                toAdd.innerHTML += '' +
                    '<div class="item">' +
                    '<div id="tableContent" class="container-fluid">' +
                    '<div class="row">'+
                    ' <div class="table-responsive">'+
                    '<table class="table table-hover" id="'+tableId+'Table">'+
                    '<thead>'+
                    '<tr id="'+tableId+'Columns">'+
                    // add table columns here
                    ' </tr>'+
                    '</thead>'+
                    '<tbody id="'+tableId+'TableBody">' +

                    '</tbody>'+
                    '</table>'+
                    '</div>'+
                    ' </div>'+
                    ' </div>'+
                    '</div>';

                $('#trigger').click();
                document.getElementById("trigger").removeAttribute("id");

            }

        }

        Table.createColumns(Table.currentTable.columns,tableId);


    } ,


    EXPEDITIONS : {
        id : "expeditions",
        item : {},
        prefix : "EX",
        editModeId : undefined,
        columns : [
            "check","action","description","type" , "club" , "startDate" , "finishedDate" , "informer" ,
            "leader" , "riggings" , "cavers" , "equipment"
        ]
        , render : function () {
            Table.renderTable();
        }
    },

    MEMBERS : {
        id : "members",
        item : {},
        prefix : "ME",
        editModeId : undefined,
        columns : [
            "check","action","username" , "firstName" , "lastName" , "expeditions"
        ]
        , render : function () {
            Table.renderTable();
        }
    } ,

    ENTRANCES : {
        id : "entrances",
        item : {},
        prefix : "EN",
        editModeId : undefined,
        columns : [
            "check", "action","isMain" , "orientation" , "size" , "latitude",
            "longitude", "photo"
        ]
        , render : function () {
            Table.renderTable();
        }

    },

    ROOMS : {
        id : "rooms",
        item : {},
        prefix : "RM",
        editModeId : undefined,
        columns : [
            "check", "action","description","name","latitude" , "longitude" ,"depth", "speleothems" , "organisms",
             "usage", "sketch", "photo"
        ]
        , render : function () {
            Table.renderTable();
        }
    },

    CAVERS : {
        id : "cavers",
        item : {},
        prefix : "CA",
        editModeId : undefined,
        columns : [
            "check",  "action","username" , "firstName" , "lastName" , "expeditions"
        ]
        , render : function () {
            Table.renderTable();
        }
    },

    CLIMATE : {
        id : "climate",
        item : {} ,
        prefix : "CL",
        editModeId : undefined,
        columns : [
            "check", "action","temperature" , "humidity" , "airCurrents","date"
        ]
        , render : function () {
            Table.renderTable();
        }
    },

    SPELEOTHEMS : {
        id : "speleothems" ,
        item : {},
        prefix : "SP",
        editModeId : undefined,
        columns : [
            "check",   "action","description","type" , "estimatedDate" ,"usage", "expeditions" , "room", "photo"
        ]
        , render : function () {
            Table.renderTable();
        }
    },

    ORGANISMS : {
        id : "organisms" ,
        item : {},
        prefix : "OR",
        editModeId : undefined,
        columns : [
            "check", "action","description","name" , "foundDate" ,
            "type", "room", "photo","expeditions"
        ]
        , render : function () {
            Table.renderTable();
        }
    },

    RIGGINGS : {
        id : "riggings" ,
        item : {},
        prefix : "RI",
        editModeId : undefined,
        columns : [
            "check", "action","description","date","expeditions" , "equipment" , "riggingEquipment",
            "sketch"
        ],

        render : function () {
            Table.renderTable();
        }

    } ,
    PHOTOS : {
        id : "photos" ,
        prefix : "PH",
        link : undefined,
        render : function () {
            Image.render();
        }
    },
    SKETCHES : {
        id : "sketches" ,
        prefix : "SK",
        render : function () {
            Sketch.render();
        }
    },
    LOGBOOK : {
        id : "logbook" ,
        prefix : "LG",
        render : function () {
            Logbook.render();
        }
    },
    EQUIPMENT : {
        id : "Equipment" ,
        prefix : "EQ",
        render : function () {
            Equipment.render();
        }
    },
    RIGGINGEQ : {
        id : "RiggingEquipment" ,
        prefix : "RQ",
        render : function () {
            Equipment.renderRiggingEquipment();
        }
    }
};

function textInput(value,externalResource) {
    var input;
    if(value === undefined){
        input = '<input type="text" placeholder="'+this.label+'" class="form-control input-sm '+this.label+' " >';
    }
    else{
        if(value === "Stalagmite" || value === "Stalactite"
            || value === "Troglomorphism" || value === "Troglofauna" || value === "Trogloxene"){
            input = '<a href="'+externalResource+'" ><input type="text" value="'+value+'" class="form-control input-sm '+this.label+'</input></a>" >';
        }
        else {
            input = '<input type="text" value="'+value+'" class="form-control input-sm '+this.label+'</input>" >';
        }
    }

    return input;
}

function descriptionTextarea(value) {
    var input;
    if(value === undefined){
        input =  ' <textarea class=\'form-control\' id=\'description\' name=\'description\' placeholder=\'Description\'' +
            ' rows=\'1\'></textarea>';
    }
    else{
            input =  ' <textarea class=\'form-control\' id=\'description\' name=\'description\' placeholder=\'Description\'' +
                ' rows=\'1\'>'+value+'</textarea>';
    }

    return input;
}

function actionButtons(id) {
    return "<div class='btn-group actions'>" +
        "<button type='button' class='btn btn-default btn-sm' id='delete_row"+id+"' onclick='deleteListener(this)'><span class='glyphicon glyphicon-remove'></span></button>" +
        "<button type='button' class='btn btn-default btn-sm' id='edit_row"+id+"' onclick='editListener(id)'  style='display:none'><span class='glyphicon glyphicon-pencil'></span></button>" +
        "<button type='button' class='btn btn-default btn-sm' onclick='confirmListener(id)' id='conf_row"+id+"'><span class='glyphicon glyphicon-ok'></span></button>" +
        "</div>";
}

function buttonModal(id) {

    return "<a role='button' name='"+this.label+"' class='btn btn-success btn-sm buttonModals' " +
        " data-toggle='modal' href='#"+id+"Modal'>" +
        this.label
        +"</a>";
}

function popOver(id) {

    return ' <a class="btn btn-success" href="#" data-toggle="popover" ' +
        ' data-html="true" data-container="body" data-content="<div id=\''+id+'descriptionContent\'> ' +
        ' <textarea class=\'form-control\' id=\'description\' name=\'description\' placeholder=\'Add a description\'' +
        ' rows=\'5\'></textarea><br>' +
        ' <button class=\'btn btn-success\' onclick = \'Table.currentTable.item [\'description\'] = $(this).closest(\'textarea\').val();\' >Save Changes</button> </div>">Description</a>\n';
}

function checkBoxes() {
    return '<input type="checkbox" class="custom-control-input checkboxes">';
}

function changeModalTable(evt) {
    var label = evt.target.name;
    var id = evt.target.id2;

    Table.rowStack[Table.counter] = $(this).closest('tr').attr('id');

    if(label === "Rigging"){
        Table.change(Table.RIGGINGS,true,id);
    }
    else if(label === "Cavers"){
        Table.change(Table.CAVERS,true,id);
    }else if(label === "Organisms") {
        Table.change(Table.ORGANISMS, true, id);

    }
    else if(label === "Equipment"){
        Table.change(Table.EQUIPMENT,true,id);


    }
    else if(label === "Rigging equipment"){
        Table.change(Table.RIGGINGEQ,true,id);

    }
    else if(label === "Sketch"){
        Table.change(Table.SKETCHES,true,id);

    }
    else if(label === "Photo"){
        Table.change(Table.PHOTOS,true,id);

    }
    else if(label === "Speleothems"){
        Table.change(Table.SPELEOTHEMS,true,id);

    }
    else if(label === "Room"){
        Table.change(Table.ROOMS,true,id);
    }
    else if(label === "Expeditions"){
        Table.change(Table.EXPEDITIONS,true,id);

    }
}

function editListener(id){
    if(Table.currentTable.editModeId === undefined) {
        var rowId = id.replace("edit_row", "");
        document.getElementById(id).style.display = 'none';
        document.getElementById(id.replace("edit", "conf")).style.display = 'block';
        Table.currentTable.editModeId = rowId;
        $("#"+rowId).find('a').each(function() {
            var el = $(this);
            el.removeAttr('disabled');
        });
        $("#"+rowId).find("input").each(function() {
            $(this).prop('readonly', false);
        });
        $("#"+rowId).find("textarea").each(function() {
            $(this).prop('readonly', false);
        });
    }
}

function confirmListener(id) {
    var rowId = id.replace("conf_row","");
    document.getElementById(id).style.display = 'none';
    document.getElementById(id.replace("conf","edit")).style.display = 'block';

    // $("#"+rowId).find('a').each(function() {
    //     $(this).attr('disabled', true);
    // });
    // $("#description").each(function() {
    //     Table.currentTable.item [$(this).attr("id")] = $(this).val();
    // });
    $("#"+rowId).find("input[type=text]").each(function() {
        $(this).prop('readonly', true);
        Table.currentTable.item [$(this).attr("placeholder")] = $(this).val();
    });
    $("#"+rowId).find("textarea").each(function() {
        $(this).prop('readonly', true);
        Table.currentTable.item [$(this).attr("placeholder")] = $(this).val();
    });
    Table.currentTable.item ["id"] = rowId;
    Utilities.ajaxCall(Table.currentTable.item,"/api/Caves/"+User.visitedUserId+"/"+Table.currentTable.id,"POST");

    Table.currentTable.item = {};
    Table.currentTable.editModeId = undefined;
}

function deleteListener(btn) {
    Table.currentTable.item = {};
    $(btn).closest('tr').remove();
    Table.currentTable.editModeId = undefined;
}

function autocompleteText() {
    if(this.label === "Username"){
        $("."+this.label).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "/api/UserProfiles/Cavers",
                    success: function(data) {
                        var label;
                        var autocomplete = [];
                        var obj = JSON.parse(data);

                        for(var i = 0; i < obj.length; i++){
                            label = obj[i].username;
                            autocomplete.push(label);
                        }
                        response(autocomplete);
                    }
                });
            }
        });
    }
    else if(this.label === "Club"){
        $("."+this.label).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "/api/UserProfiles/CavingClubs",
                    success: function(data) {
                        var label;
                        var autocomplete = [];
                        var obj = JSON.parse(data);

                        for(var i = 0; i < obj.length; i++){
                            label = obj[i].username;
                            autocomplete.push(label);
                        }
                        response(autocomplete);
                    }
                });
            }
        });
    }
    else if(this.label === "Usage"){
        var usage = ["food conservation" , "ritual" , "refuge"];
        $("."+this.label).autocomplete({
            minLength: 0,
            source: usage
        }).focus(function() {
            $(this).autocomplete("search", "");
        });
    }
    else if(this.label === "Class"){
        var classes = ["food conservation" , "ritual" , "refuge"];
        $("."+this.label).autocomplete({
            minLength: 0,
            source: classes
        }).focus(function() {
            $(this).autocomplete("search", "");
        });
    }
    else if(this.label === "Type" && Table.currentTable.id === "speleothems" ){
        var type = ["Curtain","Stalagmite","Stalactite","other"];
        $("."+this.label).autocomplete({
            minLength: 0,
            source: type
        }).focus(function() {
            $(this).autocomplete("search", "");
        });
    }
    else if(this.label === "Type" && Table.currentTable.id === "expeditions" ){
        var exp = [ "exploration", "rigging", "photography", "speleobiology"];
        $("."+this.label).autocomplete({
            minLength: 0,
            source: exp
        }).focus(function() {
            $(this).autocomplete("search", "");
        });
    }
    else if(this.label === "Type" && Table.currentTable.id === "organisms" ){
        var or = [ 'Troglomorphism','Troglofauna','Trogloxene'];
        $("."+this.label).autocomplete({
            minLength: 0,
            source: or
        }).focus(function() {
            $(this).autocomplete("search", "");
        });
    }
}

function readModeButtons(flag) {

    if(flag === true){

        // $(".checkboxes").each(function() {
        //     $(this).hide();
        // });

       if(Table.counter >= 1){
           $("#"+Table.tableStackId[Table.counter]+"Table").find('th').first().remove();
           $("#"+Table.tableStackId[Table.counter]+"Table td:first-child").remove();
           $("#"+Table.tableStackId[Table.counter]+"Table").find('th').first().remove();
           $("#"+Table.tableStackId[Table.counter]+"Table td:first-child").remove();
       }
       else{
           $("#"+Table.currentTable.id+"Table").find('th').first().remove();
           $("#"+Table.currentTable.id+"Table td:first-child").remove();
       }

        // $(".actions").each(function() {
        //     $(this).hide();
        // });

        // $(".Actions").each(function() {
        //     $(this).hide();
        // });
    }
    else{
        $(".checkboxes").each(function() {
            $(this).show();
        });
        $(".actions").each(function() {
            $(this).show();
        });
    }
}