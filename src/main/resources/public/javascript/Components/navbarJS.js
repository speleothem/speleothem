// navbar for both logged in and no logged in users
var autocompleteSrcSearch = [];
var usernameToIdSearch = {};
var navBar = {

  currentState : undefined ,

  change : function (state) {
      this.currentState.hideContent();
      this.currentState = state;
      this.currentState.render();

  } ,

  start : function () {

      if (document.cookie.indexOf("id=") >= 0){
          this.currentState = this.LoggedIn;
          if(Utilities.cookieValue("id").substring(0,2) === "CC")
             User.currentUser = User.cavingClub;
          else if(Utilities.cookieValue("id").substring(0,2) === "CA")
              User.currentUser = User.Cavers;
      }
      else
          this.currentState = this.NoLoggedIn;

      this.currentState.render();

  } ,

    NoLoggedIn : {
      hideContent : function () {
          document.getElementById('rightPartNoLogged').style.display = 'none';
      } ,
        render : function () {

            document.getElementById('navBarBody').innerHTML = '' +
                '<ul class="nav navbar-nav navbar-right" id="rightPartNoLogged">' +
                '<li><button type="button" id="SignUp" class="btn btn-sm navbar-btn"' +
                ' data-toggle="modal" data-target="#signUpModal">Sign Up</button></li>' +
                '<li><button type="button" id="login" class="btn btn-sm navbar-btn"' +
                ' data-toggle="modal" data-target="#logginModal">Login</button></li>' +
                '</ul>';

            document.getElementById('SignUp').onclick = function () {
                Modal.create("signUp");
                Modal.change(Modal.signUpModal);
            };

            document.getElementById('login').onclick = function () {
                Modal.create("loggin");

                Modal.change(Modal.logginModal);
            };
        }
    } ,
    LoggedIn : {
        hideContent : function () {
            document.getElementById('leftPartLogged').style.display = 'none';
            document.getElementById('rightPartLogged').style.display = 'none';
        } ,
        render : function () {

            // left part
            document.getElementById('navBarBody').innerHTML = '' +
                '<ul class="nav navbar-nav navbar-left" id="leftPartLogged">' +
                '<li id="home"><a href="#" onclick="view.change(view.homePageView);" >Home</a></li>'+
                '<li id="profile"><a href="#" onclick="User.visitedUser = User.currentUser;' +
                                       'User.visitedUserId = Utilities.cookieValue(\'id\');' +
                                       'view.change(view.profilePageView);">Profile</a></li>'+
                '</ul>';

            // extra features for right part
            document.getElementById('navBarBody').innerHTML += '' +
                ' <div class="navbar-form navbar-right btn-group">'+
                '<a class="btn btn-sm btn-success dropdown-toggle" data-toggle="dropdown">Actions <span class="caret"></span></a>'+
                '<ul class="dropdown-menu">'+
                '<li><a href="#" >Edit profile</a></li>'+
                '<li><a href="#">Delete Account</a></li>'+
                '<li><a id="createCave" href="#" onclick="'+
                            ' User.currentUser = User.Cave;'+
                            ' view.change(view.signUpPageView);' +
                            '" >Create Cave</a></li>'+
                '<li class="divider"></li>'+
                '<li><a href="#" onclick="User.logout();">Log out</a></li>'+
                '</ul>'+
                '</div>';


            //    right part
            document.getElementById('navBarBody').innerHTML += '' +
                '<div class="navbar-form navbar-right">' +
                '<div class="form-group" id="rightPartLogged" style="display:inline;">'+
                '<div class="input-group">'+
                '<input type="text" id="profileSearch" class="form-control input-sm">'+
                '<span class="btn btn-sm input-group-addon" id="searchBtn">' +
                '<span class="glyphicon glyphicon-search glyphicon-search-sm"></span></span>'+
                '</div>'+
                '</div>' +
                '</div>';

            this.autocomplete();

            //profile search listener
            document.getElementById("searchBtn").onclick = function () {

                var user = document.getElementById("profileSearch").value;
                var id = usernameToIdSearch[user];
                if (id.substring(0, 2) === "CA"){
                    User.visitedUser = User.Cavers;
                    User.visitedUserId = id;
                    view.change(view.profilePageView);
                }
                else if (id.substring(0, 2) === "CC"){
                    User.visitedUser = User.cavingClub;
                    User.visitedUserId = id;
                    view.change(view.profilePageView);
                }
                else if (id.substring(0, 2) === "CV"){
                    User.visitedUser = User.Cave;
                    User.visitedUserId = id;
                    view.change(view.resultPageView);
                }

            };
                if(view.currentState === view.homePageView)
                document.getElementById("rightPartLogged").style.display = 'none';
        } ,
        autocomplete : function () {

            autocompleteSrcSearch = [];
            usernameToIdSearch = {};

            Utilities.ajaxCallGET('/api/UserProfiles').done(function(data){
                var obj = JSON.parse(data);
                for(var i in obj){
                     usernameToIdSearch[obj[i].username] = obj[i].Id;
                }
            });

            Utilities.ajaxCallGET('/api/Caves').done(function(data){
                var obj = JSON.parse(data);
                for(var i in obj){
                    if(usernameToIdSearch[obj[i].name] === undefined)
                       usernameToIdSearch[obj[i].name] = obj[i].Id;
                }
                for(var j in usernameToIdSearch){
                    autocompleteSrcSearch.push(j);
                }
            });
            $("#profileSearch").autocomplete({
                source: autocompleteSrcSearch
            });
        }
    }

};