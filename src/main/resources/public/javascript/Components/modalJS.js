var Modal = {

    currentState: undefined,
    create: function (id) {
        Modal.emptyModal.render(id);
    },

    change: function (state, id) {
        this.currentState = state;

        if(id !== undefined)
            this.currentState.id = id;

        if(Table.counter === 1){
            this.currentState.targetId = id;
            Modal.imageModal.targetId = id;
            Modal.sketchModal.targetId = id;
            Modal.equipmentModal.targetId = id;
            Modal.riggingEquipmentModal.targetId = id;
        }

        this.currentState.render();
    } ,

    emptyModal: {
        id : "emptyModal" ,
        render: function (id) {

            var modalId;

            if (id === undefined) {
                modalId = "empty";
            }
            else {
                modalId = id;
            }


            document.getElementById('modalArea').innerHTML += '' +
                '<div id="' + modalId + 'Modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">' +
                '<div class="modal-dialog modal-lg">' +
                '<div class="modal-content">' +
                '<div class="modal-header">' +
                '<a type="button" href="#" class="close" id="'+modalId+'Close" data-dismiss="modal" aria-hidden="true" >&times;</a>' +
                '<h1 class="text-center" id="' + modalId + 'ModalTitle"></h1>' +
                '</div>' +
                '<div class="modal-body" id="' + modalId + 'ModalBody">' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';
        }

    },
    carouselModal : {
        create : function (id) {
            document.getElementById("modalArea").innerHTML += '' +
                ' <div class="modal modal-carousel fade and carousel slide" id="' + id + 'Modal">'+
                '<div class="modal-dialog modal-lg modal-dialog-carousel">'+
                '<div class="modal-content modal-content-carousel">'+
                '<div class="modal-header modal-header-carousel">' +
                '<a type="button" href="#" class="close" id="'+id+'Close" aria-hidden="true" >&times;</a>' +
                '<h1 class="text-center" id="' + id + 'ModalTitle"></h1>' +
                '</div>' +
                '<div class="modal-body modal-body-carousel">'+

                '<div class="carousel-inner" id="toAddItem">'+

                '</div>' +

                '</div>'+
                '<ol class="carousel-indicators" id="toAddIndi">'+

                '</ol>'+
                '</div>'+
                '</div>'+
                '</div>';

            document.getElementById(id+"Close").addEventListener('click', deleteModal , false);
            document.getElementById(id+"Close").id = id;

        } ,

        render : function () {

        }
    },
    logginModal: {

        render: function () {
            document.getElementById('logginModalTitle').innerHTML = 'Login';
            document.getElementById('logginModalBody').innerHTML =
                '<div id="loginForm">' +
                '<div class="form-group">' +
                '<input type="text" class="form-control input-lg" placeholder="username" id="loginUsername">' +
                '</div>' +
                '<div class="form-group">' +
                '<input type="password" class="form-control input-lg" placeholder="Password" id="loginPassword">' +
                '</div>' +
                '<div class="form-group">' +
                '<button data-dismiss="modal"  class="btn btn-primary btn-lg btn-block" onclick="User.login();">Login</button>' +
                '</div>' +
                '</div>';
        },

        clean: function () {
            document.getElementById('loginForm').style.display = 'none';
        }

    },

    signUpModal: {

        render: function () {
            document.getElementById('signUpModalTitle').innerHTML = 'SignUp';
            document.getElementById('signUpModalBody').innerHTML =
                '<div id="signUpButtons">' +
                '<a data-dismiss="modal" class="btn btn-primary btn-lg btn-block" id="caverSignUp" role="button" >Caver</a>' +
                '<h4 style="text-align: center;">or</h4>' +
                '<a data-dismiss="modal" class="btn btn-primary btn-lg btn-block" id="cavingClubSignUp" role="button" >Caving Club</a>' +
                '</div>';

            document.getElementById("caverSignUp").addEventListener("click", function () {
                User.currentUser = User.Cavers;
                view.change(view.signUpPageView);
            });

            document.getElementById("cavingClubSignUp").addEventListener("click", function () {
                User.currentUser = User.cavingClub;
                view.change(view.signUpPageView);
            });
        },

        clean: function () {
            document.getElementById('signUpButtons').style.display = 'none';
        }
    },

    tb: {
        id: undefined,
        targetId : undefined,
        render: function () {

            if(Table.counter ===  1)
                Modal.carouselModal.create(this.id);
            Table.renderTable("true",this.id,this.targetId);
        }
    } ,
    imageModal : {
        id: undefined,
        targetId : undefined,
        render : function () {

            if(Table.counter ===  1)
                Modal.carouselModal.create(this.id);

            Image.render("true",this.id,this.targetId);

        }
    } ,
    sketchModal : {
        id: undefined,
        targetId : undefined,
        render : function () {

            if(Table.counter ===  1)
                Modal.carouselModal.create(this.id);

            Sketch.render("true",this.id,this.targetId);

        }
    } ,
    equipmentAdd : {
        id : undefined ,
        targetId : undefined ,
        renderContent : function () {
            return '<div class="form-group">'+
                '<label for="chooseEquipment">Choose</label>'+
                '<select class="form-control" id="chooseEquipment">'+
                '<option>Backpacks</option>'+
                '<option>Cavepacks</option>'+
                '<option>Electricity Generators</option>'+
                '<option>Lighting</option>'+
                '<option>Measuring Instruments</option>'+
                '<option>Navigation Instruments</option>'+
                '<option>Recording Tools</option>'+
                '<option>Aluminum Blankets</option>'+
                '<option>Helmets</option>'+
                '</select>'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="comment">Description:</label>'+
                '<textarea class="form-control" rows="5" id="description" style="width: 100%;"></textarea>'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="quantity">Quantity:</label>'+
                '<input type="text" class="form-control" id="quantity" style="width: 10%">'+
                '</div>'+
                '<div class="form-group">'+
                '<input type="submit" class="form-control btn btn-success btn-md" id="submitEq" onclick="Equipment.sendEquipment();">'+
                '</div>'+
                '</div>';
        } ,
        render : function () {
            document.getElementById("emptyModalBody").innerHTML = this.renderContent();
        }
    } ,
    rigging_equipmentAdd : {
        id : undefined ,
        targetId : undefined ,
        renderContent : function () {
            return  '<div class="form-group">'+
                '<label for="chooseEquipment">Choose</label>'+
                '<select class="form-control" id="chooseEquipment">'+
                '<option>Drill Batteries</option>'+
                '<option>Drills</option>'+
                '<option>Dyneemas</option>'+
                '<option>Plaques</option>'+
                '<option>Rigging Sets</option>'+
                '<option>Tapes</option>'+
                '<option>Locking Carabiners</option>'+
                '<option>Non Locking Carabiners</option>'+
                '</select>'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="comment">Description:</label>'+
                '<textarea class="form-control" rows="5" id="description" style="width: 100%;"></textarea>'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="quantity">Quantity:</label>'+
                '<input type="text" class="form-control" id="quantity" style="width: 10%">'+
                '</div>'+
                '<div class="form-group">'+
                '<input type="submit" class="form-control btn btn-success btn-md" id="submitEq" onclick="Equipment.sendEquipment();">'+
                '</div>'+
                '</div>';
        } ,
        render : function () {
            document.getElementById("emptyModalBody").innerHTML = this.renderContent();
        }
    } ,
    equipmentModal : {
        id : undefined ,
        targetId : undefined ,
        render : function () {
            if(Table.counter ===  1)
                Modal.carouselModal.create(this.id);

            Equipment.render("true",this.id,this.targetId);
        }
    } ,
    riggingEquipmentModal : {
        id : undefined ,
        targetId : undefined ,
        render : function () {
            if(Table.counter ===  1)
                Modal.carouselModal.create(this.id);

            Equipment.renderRiggingEquipment("true",this.id,this.targetId);
        }
    }
};

function deleteModal(evt) {
    var id = evt.target.id;
    var valuesArray = [];
    if(Table.currentTable.id === "photos"){
        valuesArray.push(Table.currentTable.link); //link
        valuesArray.push(Image.currentCategory); //type
        valuesArray.push(Utilities.idGenerator("PH")); //id
        valuesArray.push(Utilities.cookieValue("id")); //ownerId
        valuesArray.push(Table.tableStack[Table.counter - 1].editModeId); //entityId
        valuesArray.push(User.visitedUserId); //caveId
        Image.currentCategory = "inside";
    }
    else if(Table.currentTable.id === "sketches"){
        valuesArray.push(Table.currentTable.link); //link
        valuesArray.push(Sketch.currentCategory); //type
        valuesArray.push(Utilities.idGenerator("SK")); //id
        valuesArray.push(Utilities.cookieValue("id")); //ownerId
        valuesArray.push(Table.tableStack[Table.counter - 1].editModeId); //entityId
        valuesArray.push(User.visitedUserId); //caveId
        Sketch.currentCategory = "rigging";
    }
    else if(Table.currentTable.id === "RiggingEquipment" || Table.currentTable.id === "Equipment"){
        if(Table.tableStack[Table.counter - 1].editModeId !== undefined){
            valuesArray.push(document.getElementById("description").value);
            valuesArray.push(document.getElementById("quantity").value);
            valuesArray.push(document.getElementById("chooseEquipment").value);
            valuesArray.push( Utilities.idGenerator("EQ"));
        }
    }
    else{
        $("#"+this.id+"Table").find('input[type="checkbox"]:checked').each(function () {
            valuesArray.push($(this).closest("tr").attr('id'));
        });
    }

    Table.tableStack[0].item[Table.currentTable.id] = valuesArray;
    $('.modal').modal('hide');
    var elem = document.querySelector('#'+id+"Modal");
    elem.parentNode.removeChild(elem);
    Table.currentTable.editModeId = undefined;
    for(var i = 1;i < Table.counter; i++){
        Table.tableStack[i].editModeId = undefined;
    }
    Table.counter = 0;
    Table.currentTable = Table.tableStack[Table.counter];
    if(User.visitedUser.name === "CavingClubs" || User.visitedUser.name === "Cavers"){
        readModeButtons(true);
        // hideAction();
    }
    else{
        readModeButtons(false);
    }
}

function hideAction() {
    var t = document.getElementById(Table.currentTable.id+"Table");
    t.getElementsByTagName("th")[0].style.display = "none";
}