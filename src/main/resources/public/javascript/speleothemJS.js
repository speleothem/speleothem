var view = {

  currentState : undefined ,

  create :  function(){
      this.currentState = this.homePageView;
      this.currentState.render();
  } ,

  change : function(changeState) {
        this.currentState.clean();
        this.currentState = changeState;
        navBar.currentState.render();
        this.currentState.render();
  },

    homePageView : {
        render : function () {
            document.body.style.background = '-webkit-radial-gradient(circle, #31427E, #071116)';
            document.getElementById('main').innerHTML = '' +
                '<div class="row" id="homePage">'+
                '<div class="col-md-4 col-center-block">'+
                '<img src="Files/logo_big.png">'+
                '<div class="form-group" style="display:inline;">'+
                '<div class="input-group">'+
                '<input id="location_search" type="text" class="form-control input-sm">'+
                '<span class="btn btn-sm input-group-addon"><span class="glyphicon glyphicon-search glyphicon-search-sm"></span></span>'+
                '</div>'+
                '</div>'+
                '</div>'+
                '</div>';
        },
        clean : function () {
            document.getElementById('homePage').style.display = 'none';
        }
    },

    signUpPageView : {
        render : function () {
            document.body.style.background = 'none';
            document.getElementById('main').innerHTML = '' +
                '<div class="container-fluid" id="signUpPage">'+
                '<div class="row">'+
                '<div class="col-md-8">'+
                '<h1>Sign Up</h1>'+
                '<hr>' +
                '<div class="form-horizontal" id="signUpForm"></div>' +
                '</div>' +
                '</div>' +
                '</div>';
            User.signUpRender();
            User.currentUser.autocomplete();
        },
        clean : function () {
            document.getElementById('signUpPage').style.display = 'none';
        }
    } ,

    profilePageView : {
        render : function () {
            document.body.style.background = 'none';
            document.getElementById('profilePage').innerHTML = '<div id="proPage">' +
                    '<br>' +
                    '<ul class="nav nav-tabs" id="tabs">' +
                    '</ul>' +
                     '<div class="tab-content" id="tabContent">' +
                    '</div>' +
                    '</div>';

            User.profileRender();
        } ,
        clean : function () {
            document.getElementById('proPage').style.display = 'none';
        }
    } ,

    resultPageView : {
      map : undefined ,
      markers : [] ,
      resultId : undefined ,
      init : function () {
          document.getElementById("main").style.width = '100%';
          document.getElementById("main").style.height = '91%';
          document.body.style.background = 'none';
          document.getElementById("main").innerHTML += '<div class="col-md-5" id="results_boxes"></div>';

          document.getElementById("main").innerHTML += '<div id="map" style="float:right;width:58%;height:98%;"></div>';

          map = L.map('map');
          L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
          }).addTo(map);
      } ,
      createResult : function (obj,counter) {

          return  "<div class='panel panel-primary result-panel'>"+
                  "<div class='panel-heading clickable'>"+
                  "<h3 class='panel-title'><a id='"+User.visitedUserId+"'>"+ obj.name +"</a></h3>"+
                  "<span class='pull-right '><i class='fa fa-minus-square'></i></span>"+
                  "</div>"+
                  "<div class='panel-body result-body' id='body_"+counter+"'>"+
                  // "<img class='img-thumbnail' align='right' src='../Files/images.jpeg'><p>"+ obj.description +"</p></div>"+
                  "<img class='img-thumbnail' align='right' src='../Files/default-profile-picture.png'><p>"+ obj.description +"</p></div>"+
                  "</div>" ;

      } ,
      createMarker : function (obj) {
          var text = obj.location + " , " + obj.city;
        return L.marker([obj.latitude, obj.longitude]).addTo(map)
            .bindPopup(text);
      } ,
      addListeners : function (counter,id) {
          $("[id='body_" + counter + "']").hover(function(){
              view.resultPageView.markers[counter].openPopup();
          });
          $("[id='body_" + counter + "']").mouseleave(function(){
              view.resultPageView.markers[counter].closePopup();
          });
          $("[id='" + id + "']").hover(function(){
              view.resultPageView.markers[counter].openPopup();
          });
          $("[id='" + id + "']").mouseleave(function(){
              view.resultPageView.markers[counter].closePopup();
          });
          $("[id='" + id + "']").click(function(){
              User.visitedUser = User.Cave;
              User.visitedUserId = id;
              view.change(view.profilePageView);
          });
      } ,
      render : function () {
          this.init();
          Utilities.ajaxCallGET('/api/Caves/' + User.visitedUserId + '/general').done(function(data){
              var obj = JSON.parse(data);
              var counter = 0;

              // for(var i = 0 ; i < obj.length; i++){
                  document.getElementById('results_boxes').innerHTML += view.resultPageView.
                                                        createResult(obj,counter);
                  view.resultPageView.markers.push(view.resultPageView.createMarker(obj));
                  view.resultPageView.addListeners(counter,User.visitedUserId);
                  // counter++;
              // }
              var group = new L.featureGroup(view.resultPageView.markers);
              map.fitBounds(group.getBounds());
          });

        } ,

        clean : function () {
            document.getElementById('results_boxes').remove();
            document.getElementById('map').remove();
            document.getElementById("main").style.width = '';
            document.getElementById("main").style.height = '';
        }

    }
};

$(document).on('click', '.panel-heading span.clickable', function (e) {
    var $this = $(this);
    if (!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.addClass('panel-collapsed');
        $this.find('i').removeClass('fa fa-minus-square').addClass('fa fa-plus-square');
    } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.find('i').removeClass('fa fa-plus-square').addClass('fa fa-minus-square');
    }
});
$(document).on('click', '.panel div.clickable', function (e) {
    var $this = $(this);
    if (!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.addClass('panel-collapsed');
        $this.find('i').removeClass('fa fa-minus-square').addClass('fa fa-plus-square');
    } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.find('i').removeClass('fa fa-plus-square').addClass('fa fa-minus-square');
    }
});
$(document).ready(function () {
    $('.panel-heading span.clickable').click();
    $('.panel div.clickable').click();
});